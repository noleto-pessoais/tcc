<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get	("/login",	"Auth\LoginController@showLoginForm")->name("login");
Route::post	("/login",	"Auth\LoginController@login");

Route::get	("/register",	"Auth\RegisterController@showRegistrationForm")->name("register");
Route::post	("/register",	"Auth\RegisterController@register");

Route::get("test", "ClassController@test");

Route::group(['middleware' => ["auth"]], function() {

    
    Route::post("/logout",	"Auth\LoginController@logout");
    
    /* Route::get      ("turmas",             "ClassController@index");
    Route::get      ("turmas/criar",       "ClassController@create");
    Route::get      ("turmas/{id}/editar", "ClassController@edit");
    Route::get      ("turmas/{id}/calendario", "ClassController@calendar");
    
    Route::post     ("turma",             "ClassController@save");
    Route::put      ("turma",             "ClassController@save");
    Route::delete   ("turma",             "ClassController@delete"); */
    
    Route::get("/", function() {
        return redirect("turmas");
    });

    Route::get      ('/turmas', "CourseController@index");
    Route::get      ('/turmas/lembretes', 'CourseController@reminder');
    Route::post     ("/turma", "CourseController@save");
    Route::delete   ("/turma", "CourseController@delete");
    Route::get      ('/turma/{id}', "CourseController@details");
    Route::get      ('/turma/{id}/qrcode', "CourseController@qrcode");
    Route::get      ('/qrcode/{id}', "CourseController@getQrcodeImage");

});


