/**
 * Javascript do helper "Alert"
 */

var closeAlert = function($elem) {

	setTimeout(function() {

	 	$elem.removeClass('fadeInDown').addClass('fadeOutUp');

	 	setTimeout(function() {
			$elem.remove();
	 	}, 500);

	}, 5000);
}

var $alert;

$(function() {

	$('.alert:not(.sticky)').each(function() {
		closeAlert($(this));
	});

	var toggle = function(type, message, $container) {
		
		var $container = $container || $('.alert-container').first();
		
		var html =
		'<div class="alert alert-'+type+' animated fadeInDown">'+
			'<div class="texts"><p>'+message+'</p></div><i class="close-icon fas fa-times"></i>'+
		'</div>';

		var $elem = $(html);

		$elem.find(".close-icon").on("click", function() {
			$elem.remove();
		});

		closeAlert($elem);

		$('html, body').animate({ scrollTop: 0 }, 'slow');
		$container.append($elem);
		
	}

	$alert = {

		success: function(message, $container) {
			toggle('success', message, $container);
		},

		info: function(message, $container) {
			toggle('info', message, $container);
		},

		error: function(message, $container) {
			toggle('danger', message, $container);
		},

		warning: function(message, $container) {
			toggle('warning', message, $container);
		}
	};

	var $sessionAlerts = $(".alert-data").detach();
	$(".session-alerts").remove();

	var sessionAlertIcons = {
		success: '<i class="fas fa-check-circle"></i>',
		error: '<i class="fas fa-times-circle"></i>',
		warning: '<i class="fas fa-exclamation-circle"></i>'
	};

	$sessionAlerts.each(function() {

		var type = $(this).data("type");
		var message = $(this).html();

		var html = `
		<div class="session-alert-content">
			<div class="session-alert-head">
				<div class="icon">${ sessionAlertIcons[type] }</div>
				<h3 class="title">${ type == 'success' ? 'Sucesso!' : 'Atenção!' }</h3>
			</div>
			<div class="session-alert-text"></div>
		</div>`;

		var $html = $(html);
		$html.find(".session-alert-text").html(message);

		bootbox.dialog({
			size: "small",
			className: `session-alert-modal ${ type }`,
			message: $html,
			closeButton: false,
			buttons: { confirm: { label: "OK" } }
		});

	});

});
