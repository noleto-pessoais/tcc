"use strict";

$(function() {

    $("form[data-parsley-validate]").each(function() {
        createFormErrorsContainer($(this));
    });
    
});

function createFormErrorsContainer($form) {

    $form.find(".form-control[required]").each(function() {

        var $control = $(this);
        var $parent = $control.parent();

        if (!$control.attr("data-parsley-errors-container")) {

            var name = $control.attr("name") + "_error";
            $control.attr("data-parsley-errors-container", '#' + name);

            var $div = $(`<div id="${ name }"></div>`);
            $parent.find("label").after($div);

        }
    });
}