"use strict";

/**
 * Bloquear o elemento (abrir loading)
 */
$.fn.wait = function(text) {
    $(this).append('<div class="loader-container"><div class="loader-content"><div class="loader"></div></div></div>');
}

/**
 * Desbloquear o elemento (remover loading)
 */
$.fn.closeWait = function() {
    $(this).find(".loader-container").fadeOut(300, function() {
        $(this).remove();
    });
};

function wait() {
    $("body").wait();
}

function closeWait() {
    $("body").closeWait();
}