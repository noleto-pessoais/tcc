"use strict";

$(document).ready(function() {

    $(".btn-logout").on("click", function(e) {
		e.preventDefault();
		submitCustomForm("post", "/logout", { _token: meta("csrf") });
    });

    $(".selectpicker").each(function() {

        var $select = $(this);
        var $group = $select.closest(".form-group");

        $select.selectpicker({
            dropupAuto: false,
            size: 5
        });

    });

    $(".table").each(function() {

        var $table = $(this);
        var $tbody = $(this).find("tbody");

        var $dropdown = $(".table-action-dropdown");

        $dropdown.on("shown.bs.dropdown", function() {

            $table.addClass("has-active");
            $tbody.children().removeClass("active");

            $(this).closest("tr").addClass("active");

        }).on("hide.bs.dropdown", function() {

            $table.removeClass("has-active");
            $tbody.children().removeClass("active");

        });

    });

    $(".dropdown-toggle").each(function() {
        if (!isMobile.any()) $(this).attr("data-boundary", "window");
	});
	
	$(".btn-remove").on("click", function(e) {

		e.preventDefault();

		var action = $(this).data("url");
		var id = $(this).data("id");

		if (action && id) {
			submitCustomForm("post", action, { _method: "delete", _token: meta("csrf"), id: id });
		}

		return false;

	});

	$("table.datatable").each(function() {
       var table = loadDatatable(this);
    });

});

function loadDatatable(table, customOptions) {

	var language = {
		"emptyTable": "Nenhum registro encontrado",
		"info": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		"infoEmpty": "Mostrando 0 até 0 de 0 registros",
		"infoFiltered": "(Filtrados de _MAX_ registros)",
		"infoThousands": ".",
		"loadingRecords": "Carregando...",
		"processing": "Processando...",
		"zeroRecords": "Nenhum registro encontrado",
		"search": "Pesquisar",
		"paginate": {
			"next": "Próximo",
			"previous": "Anterior",
			"first": "Primeiro",
			"last": "Último"
		},
		"aria": {
			"sortAscending": ": Ordenar colunas de forma ascendente",
			"sortDescending": ": Ordenar colunas de forma descendente"
		},
		"select": {
			"rows": {
				"_": "Selecionado %d linhas",
				"0": "Nenhuma linha selecionada",
				"1": "Selecionado 1 linha"
			},
			"1": "%d linha selecionada",
			"_": "%d linhas selecionadas",
			"cells": {
				"1": "1 célula selecionada",
				"_": "%d células selecionadas"
			},
			"columns": {
				"1": "1 coluna selecionada",
				"_": "%d colunas selecionadas"
			}
		},
		"buttons": {
			"copySuccess": {
				"1": "Uma linha copiada com sucesso",
				"_": "%d linhas copiadas com sucesso"
			},
			"collection": "Coleção  <span class=\"ui-button-icon-primary ui-icon ui-icon-triangle-1-s\"><\/span>",
			"colvis": "Visibilidade da Coluna",
			"colvisRestore": "Restaurar Visibilidade",
			"copy": "Copiar",
			"copyKeys": "Pressione ctrl ou u2318 + C para copiar os dados da tabela para a área de transferência do sistema. Para cancelar, clique nesta mensagem ou pressione Esc..",
			"copyTitle": "Copiar para a Área de Transferência",
			"csv": "CSV",
			"excel": "Excel",
			"pageLength": {
				"-1": "Mostrar todos os registros",
				"1": "Mostrar 1 registro",
				"_": "Mostrar %d registros"
			},
			"pdf": "PDF",
			"print": "Imprimir"
		},
		"autoFill": {
			"cancel": "Cancelar",
			"fill": "Preencher todas as células com",
			"fillHorizontal": "Preencher células horizontalmente",
			"fillVertical": "Preencher células verticalmente"
		},
		"lengthMenu": "Exibir _MENU_ resultados por página",
		"searchBuilder": {
			"add": "Adicionar Condição",
			"button": {
				"0": "Construtor de Pesquisa",
				"_": "Construtor de Pesquisa (%d)"
			},
			"clearAll": "Limpar Tudo",
			"condition": "Condição",
			"conditions": {
				"date": {
					"after": "Depois",
					"before": "Antes",
					"between": "Entre",
					"empty": "Vazio",
					"equals": "Igual",
					"not": "Não",
					"notBetween": "Não Entre",
					"notEmpty": "Não Vazio"
				},
				"number": {
					"between": "Entre",
					"empty": "Vazio",
					"equals": "Igual",
					"gt": "Maior Que",
					"gte": "Maior ou Igual a",
					"lt": "Menor Que",
					"lte": "Menor ou Igual a",
					"not": "Não",
					"notBetween": "Não Entre",
					"notEmpty": "Não Vazio"
				},
				"string": {
					"contains": "Contém",
					"empty": "Vazio",
					"endsWith": "Termina Com",
					"equals": "Igual",
					"not": "Não",
					"notEmpty": "Não Vazio",
					"startsWith": "Começa Com"
				}
			},
			"data": "Data",
			"deleteTitle": "Excluir regra de filtragem",
			"logicAnd": "E",
			"logicOr": "Ou",
			"title": {
				"0": "Construtor de Pesquisa",
				"_": "Construtor de Pesquisa (%d)"
			},
			"value": "Valor"
		},
		"searchPanes": {
			"clearMessage": "Limpar Tudo",
			"collapse": {
				"0": "Painéis de Pesquisa",
				"_": "Painéis de Pesquisa (%d)"
			},
			"count": "{total}",
			"countFiltered": "{shown} ({total})",
			"emptyPanes": "Nenhum Painel de Pesquisa",
			"loadMessage": "Carregando Painéis de Pesquisa...",
			"title": "Filtros Ativos"
		},
		"searchPlaceholder": "Digite um termo para pesquisar",
		"thousands": "."
	};

	var options = {
		lengthChange: false,
		searching: false,
		language: language,
		columnDefs: [
			{ "targets": 'no-sort', "orderable": false }
		]
	};

	if (customOptions) {
		options = Object.assign(options, customOptions);
	}

	return $(table).DataTable(options);
}

/**
 * Verifica se o usuário está acessando pelo modo mobile
 */
var isMobile = {
	Android : function() {
		return navigator.userAgent.match(/Android/i);
	},
	BlackBerry : function() {
		return navigator.userAgent.match(/BlackBerry/i);
	},
	iOS : function() {
		return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	},
	Opera : function() {
		return navigator.userAgent.match(/Opera Mini/i);
	},
	Windows : function() {
		return navigator.userAgent.match(/IEMobile/i);
	},
	any : function() {
		return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	}
};

/**
 * Obter conteúdo de um meta
 * @param {*} name 
 */
function meta(name) {

	var $element = $(`meta[name="${ name }"]`);

	if ($element.length > 0) {
		return $element.attr("content");

	} else {
		return null;
	}
}

/**
 * Retornar mensagem que deve ser exibida em um erro ajax
 * @param {*} err
 * @returns string
 */
function ajaxErrMessage(err) {

	var message = "Erro interno";

	if (!err.responseJSON && err.responseText) {
		message = err.responseText;

	} else if (err.responseJSON && err.responseJSON.message) {
		message = err.responseJSON.message;

	} else if (err.statusText) {

		if (err.status == 404) {
			message = "URL de requisição inválida";

		} else {
			message = err.statusText;
		}
	}

	return message;
}

/**
 * Submeter formulário criado dinamicamente
 * @param {*} method 
 * @param {*} action 
 * @param {*} body 
 * @param {*} submit 
 */
function submitCustomForm(method, action, body) {

	var $form = $("<form></form>");

	$form.attr("method", method);
	$form.attr("action", meta("url") + action);

	$.each(body, function(key, value) {
		$form.append(`<input type="hidden" name="${ key }" value="${ value }" />`);
	});

	$("body").append($form);

	wait();
	$form.trigger("submit");
}

/**
 * Retornar JSON dentro de determinado elemento [data-json]
 * @param {*} name 
 */
function dataJson(name) {

	try {

		var $element = $(`[data-json="${name}"]`);
		if ($element.length > 0) {

			$element.remove();

			var json = $element.attr("value");
			var data = JSON.parse(json);

			if (data) {
				return data;
			}
		}

	} catch (exception) {
		console.error(`dataJson #${name} error`, exception);
	}
}
