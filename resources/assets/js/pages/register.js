"use strict";

$(function() {

    $(".page.register").each(function() {

        var $page = $(this);
        var $form = $(this).find("form");
        var $matriculation = $form.find('[name="matriculation"]');
        var $program = $form.find('[name="program_id"]');

        var seed = {
            name: "Victor Noleto",
            program_id: 1,
            matriculation: "201503144",
            email: "victornoleto@discente.ufg.br",
            login: "victornoleto",
            password: "123123"
        };

        for (var key in seed) {
            //$form.find(`[name="${key}"]`).val(seed[key]);
        }

        $form.find('[name="group_id"]').on("change", function() {

            var value = this.value;

            $page.find(".group-image").addClass("d-none");
            $page.find(`.group-image[data-id="${ value }"]`).removeClass("d-none");

            // Professor
            if (value == 2) {

                $matriculation.val("").removeAttr("required");
                $matriculation.parent().addClass("d-none");

                $program.val("").removeAttr("required");
                $program.parent().addClass("d-none");

            // Aluno
            } else if (value == 3) {

                $matriculation.attr("required", "required");
                $matriculation.parent().removeClass("d-none");

                $program.attr("required", "required");
                $program.parent().removeClass("d-none");
            }

        });

    });

    $(".page.login").each(function() {

        var $form = $(this).find("form");

        var seed = {
            login: "victornoleto",
            password: "123123"
        };

        for (var key in seed) {
            //$form.find(`[name="${key}"]`).val(seed[key]);
        }

    });

});