"use strict";

$(function() {

    $(".page.page-courses.page-index:not(.page-students-index)").each(function() {

        var $page = $(this);
        var $table = $page.find(".table");
        var $addButton = $page.find(".btn-add");
        var $coursesSummary = $page.find(".courses-summary");

        var programs = dataJson("programs");

        function loadCoursesSummary() {

            $coursesSummary.find(".card").wait();
            $coursesSummary.find(".card").removeAttr("data-course");
            $coursesSummary.find(".card").off("click");

            var url = meta("url") + "/turmas/lembretes";

            $.get(url, function(response) {

                $.each(response, function(key, data) {

                    var $card = $coursesSummary.find(`.card[data-key="${key}"]`);
                    var $text = $card.find(".text");

                    if (data) {

                        $card.attr("data-course", data.id);

                        $card.on("click", function() {
                            wait();
                            window.location.href = meta("url") + "/turma/" + data.id;
                        });

                        var date = moment(data.date + " " + data.start_at);

                        $text.html(`
                            <h3>${ data.name }</h3>
                            <small>${ data.program_name }</small>
                            <small>${ data.schedule_key} - ${ date.format('DD/MM HH:mm') }</small>
                        `);

                    } else {
                        $text.html(`<h3>Nenhuma</h3>`);
                    }

                });

                $coursesSummary.find(".card").closeWait();

            });
        }

        loadCoursesSummary();

        function openClassFormModal(data) {

            var html = `
            <div class="class-form">
                <div class="alert-container"></div>
                <form>
                    <div class="form-group">
                        <label>Nome</label>
                        <input type="text" class="form-control" name="name" required />
                    </div>
                    <div class="form-group">
                        <label>Curso</label>
                        <select class="form-control" name="program_id" required>
                            <option value="">Selecione</option>
                        </select>
                    </div>
                    <div class="form-group mb-1">
                        <label>Horário</label>
                        <input type="text" class="form-control" name="schedule_key" required />
                    </div>
                </form>
            </div>`;

            var $html = $(html);

            programs.forEach(function(p) {
                $html.find(`[name="program_id"]`).append(`<option value="${ p.id }">${ p.name }</option>`);
            });

            var $form = $html.find("form");
            createFormErrorsContainer($form);

            if (data) {

                for (var key in data) {
                    $form.find(`[name="${ key }"]`).val(data[key]);
                }
            }

            var $alertContainer = $html.find(".alert-container");

            function save() {

                if ($form.parsley().validate()) {

                    var body = {
                        _token: meta("csrf")
                    }

                    if (data && data.id) {
                        body.id = data.id
                    }

                    $form.find(".form-control").each(function() {
                        body[this.getAttribute("name")] = this.value
                    });

                    var successCallback = function(response) {
                        location.reload();
                    }

                    var errorCallback = function(err) {
                        $alert.error("Não foi possível salvar a turma:<br/><b>" + ajaxErrMessage(err) + "</b>", $alertContainer);
                    }

                    $.ajax({
                        method: "POST",
                        url: meta("url") + "/turma",
                        data: body,
                        success: successCallback,
                        error: errorCallback
                    });

                } else {
                    $alert.error("Verifique os erros no formulário", $alertContainer);
                }

                return false;
            }

            var modal = bootbox.dialog({
                title: "Formulário de Turma",
                message: $html,
                closeButton: false,
                buttons: {
                    close: { label: "Fechar", className: "btn-outline-primary" },
                    save: { label: "Salvar", className: "btn-primary", callback: save },
                }
            });
        }

        $addButton.on("click", function() {
            openClassFormModal();
        });

        $table.find(".btn-update").on("click", function() {
            openClassFormModal($(this).closest("tr").data("course"));
        });

        $table.find(".btn-delete").on("click", function(e) {

            var course = $(this).closest("tr").data("course");

            var html = `
            <div class="session-alert-content">
                <div class="session-alert-head">
                    <div class="icon">
                        <i class="fas fa-exclamation-circle"></i>
                    </div>
                    <h3 class="title"Atenção!</h3>
                </div>
                <div class="session-alert-text">
                    <p>Você tem certeza que deseja remover a turma <b>${ course.name }</b>?</p>
                    <p class="mt-2">Essa é uma ação irreversível!</p>
                </div>
            </div>`;
            
            var callback = function() {

                var body = {
                    _method: "delete", _token: meta("csrf"), id: course.id
                };

                submitCustomForm("post", "/turma", body);
            }

            var box = bootbox.dialog({
                size: "small",
                className: `session-alert-modal warning`,
                message: html,
                closeButton: false,
                buttons: {
                    cancel: { label: "Cancelar", className: "btn-outline-primary" },
                    confirm: { label: "Confirmar", callback: callback }
                }
            });
    
        });

        $table.find("tbody tr td:not(:last-child)").on("click", function() {

            var $tr = $(this).parent();
            var id = $tr.find(`td[data-key="id"]`).text();

            wait();
            window.location.href = meta("url") + "/turma/" + id;
            
        });

    });
    
    $(".page.page-courses.page-details").each(function() {
        
        var $page = $(this);
        
        $page.find(".students-table").each(function() {
            
            var $table = $(this);

            function openStudentData(data) {
    
                var rate = data.summary.attendance_rate;
                rate = rate.toLocaleString('pt-br');
    
                var html = `
                <div class="students-data-wrapper">
    
                    <div class="cards courses-summary">
                        <div class="card card-body" data-key="current">
                            <h6>Presenças</h6>
                            <div class="text">
                                <h3>${ data.summary.attendance }</h3>
                                <small>${ data.summary.attendance }/${ data.summary.total }</small>
                            </div>
                        </div>
                        <div class="card card-body" data-key="next">
                            <h6>Faltas</h6>
                            <div class="text">
                                <h3>${ data.summary.absence }</h3>
                                <small>${ data.summary.absence }/${ data.summary.total }</small>
                            </div>
                        </div>
                        <div class="card card-body" data-key="last">
                            <h6>Percentual de presença</h6>
                            <div class="text">
                                <h3>${ rate }%</h3>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-actions attendances-table bigger-padding">
                            <thead>
                                <tr>
                                    <th>Aula</th>
                                    <th class="no-sort">Presença</th>
                                    <th>Presença</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>`;
    
                var $html = $(html);
                var $table = $html.find("table");
    
                for (var date in data.attendances) {
    
                    let row = data.attendances[date];
    
                    var d1 = moment(date);
                    var d1str = d1.format("DD/MM/YY");
                    var d2 = row.date ? moment(row.date).format("DD/MM/YY HH:mm") : "-";
    
                    var tr = `
                    <tr>
                        <td data-sort="${ d1.format("YYYY-MM-DD")}">${ d1str }</td>
                        <td>${ d2 }</td>
                        <td data-sort="${ row.attendance }">
                            <div class="student-attendance-info">
                                <span>${ row.attendance }/2</span>
                                <span class="dot ${ 'dot-' + row.attendance }"></span>
                            </div>
                        </td>
                    </tr>`;
    
                    $table.find("tbody").prepend(tr);
                }
    
                // Carregar datatable
                loadDatatable($table[0], { "order": [[0, "desc"]] });

                var headerHtml = `
                <div class="custom-modal-header">
                    <div class="title">
                        <p>${ data.name }</p>
                        <small>${ data.matriculation }</small>
                    </div>
                    <div class="controls">
                        <button type="button" class="btn btn-primary">Atualizar</button>
                        <button type="button" class="btn btn-light btn-close">Fechar</button>
                    </div>
                </div>`;

                var $headerHtml = $(headerHtml);

                $headerHtml.on("click", ".btn-close", function() {
                    bootbox.hideAll();
                });
    
                var box = bootbox.dialog({
                    title: $headerHtml,
                    message: $html,
                    className: "students-data-modal",
                    size: "large",
                    closeButton: false,
                });
            }
    
            $table.find(".open-student-data").on("click", function() {
                var $tr = $(this).closest("tr");
                var data = $tr.data("student");
                openStudentData(data);
            });
            
        });

        $page.find(".dates-table").each(function() {

            var $datesData = $(`textarea[name="dates-data"]`).detach();
            var data = JSON.parse($datesData.val());

            var $table = $(this);
            var $tr = $table.find("tbody tr");

            var today = moment().format("YYYY-MM-DD");

            var findIndex = Array.from($tr).findIndex(function(tr) {
                return $(tr).find(`.date-column`).attr('data-sort') >= today;
            });

            var dt = loadDatatable($table[0]);
            dt.page(Math.ceil(findIndex / 10) - 1).draw(false);

            function openDateDetails(key) {

                var dateData = data[key];
                var dateKey = moment(key);

                var rate = dateData.summary.attendances_rate;
                rate = rate.toLocaleString('pt-br');

                var html = `
                <div class="dates-data-wrapper">

                    <div class="cards courses-summary">
                        <div class="card card-body" data-key="current">
                            <h6>Presenças</h6>
                            <div class="text">
                                <h3>${ dateData.summary.students_attendance }</h3>
                                <small>${ dateData.summary.students_attendance }/${ dateData.summary.students_count }</small>
                            </div>
                        </div>
                        <div class="card card-body" data-key="next">
                            <h6>Faltas</h6>
                            <div class="text">
                                <h3>${ (dateData.summary.students_count - dateData.summary.students_attendance) }</h3>
                                <small>${ (dateData.summary.students_count - dateData.summary.students_attendance) }/${ dateData.summary.students_count }</small>
                            </div>
                        </div>
                        <div class="card card-body" data-key="last">
                            <h6>Percentual de presença</h6>
                            <div class="text">
                                <h3>${ rate }%</h3>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-actions table-fixed attendances-table bigger-padding">
                            <thead>
                                <tr>
                                    <th>Aluno</th>
                                    <th>Presença</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>`;

                var $html = $(html);
                var $table = $html.find("table");

                for (var matriculation in dateData.students) {

                    let row = dateData.students[matriculation];
                    var date = row.date ? moment(row.date).format("HH:mm") : "-";

                    var tr = `
                    <tr>
                        <td>${ row.name }<small class="d-block">${ row.matriculation }</small></td>
                        <td>${ date }</td>
                        <td data-sort="${ row.attendance }">
                            <div class="student-attendance-info">
                                <span>${ row.attendance }/2</span>
                                <span class="dot ${ 'dot-' + row.attendance }"></span>
                            </div>
                        </td>
                    </tr>`;

                    $table.find("tbody").prepend(tr);
                }

                // Carregar datatable
                loadDatatable($table[0]);

                var headerHtml = `
                <div class="custom-modal-header">
                    <div class="title">
                        <p>${ dateKey.format('ll') }</p>
                        <small>18h50 às 20h20</small>
                    </div>
                    <div class="controls">
                        <button type="button" class="btn btn-primary btn-refresh">Atualizar</button>
                        <button type="button" class="btn btn-outline-primary btn-automatic-refresh">Ativar atualização automática</button>
                        <button type="button" class="btn btn-light btn-close">Fechar</button>
                    </div>
                </div>`;

                var $headerHtml = $(headerHtml);

                $headerHtml.on("click", ".btn-close", function() {
                    bootbox.hideAll();
                });

                $headerHtml.on("click", ".btn-automatic-refresh", function() {
                    
                    var $btn = $(this);
                    console.log("oi");

                    if ($btn.hasClass("btn-primary")) {
                        $btn.text("Ativar atualização automática");
                        $btn.addClass("btn-outline-primary").removeClass("btn-primary");
                        $headerHtml.find(".btn-refresh").removeAttr("disabled");
                        $headerHtml.find(".btn-refresh").removeClass("btn-outline-primary").addClass("btn-primary");

                    } else {
                        $btn.text("Desativar atualização automática");
                        $btn.addClass("btn-primary").removeClass("btn-outline-primary");
                        $headerHtml.find(".btn-refresh").attr("disabled", "disabled");
                        $headerHtml.find(".btn-refresh").removeClass("btn-primary").addClass("btn-outline-primary");
                    }
                    
                });

                var box = bootbox.dialog({
                    title: $headerHtml,
                    message: $html,
                    className: "students-data-modal",
                    size: "large",
                    closeButton: false,
                });

            };

            $table.on("click", ".open-date-data", function() {
                openDateDetails($(this).closest("tr").find(".date-column").attr("data-sort"));
            });

        });

        $page.find(".marcar-presenca-btn").each(function() {

            var $btn = $(this);
            
            var $wrapper = $page.find(".marcar-presenca-wrapper").detach();
            $wrapper.removeClass("d-none");

            function openMarcarPresencaModal() {

                var $wrapperClone = $wrapper.clone();
                var $linhaVazia = $wrapperClone.find("tbody tr").first().clone();

                $wrapperClone.find(".add-nova-linha").on("click", function() {
                    $wrapperClone.find("table tbody").append($linhaVazia.clone());
                });

                $wrapperClone.on("click", "tbody tr .duplicate", function() {
                    var $tr = $(this).closest("tr"); $tr.after($tr.clone());
                });

                $wrapperClone.on("click", "tbody tr .remove", function() {
                    $(this).closest("tr").remove();
                });

                var headerHtml = `
                <div class="custom-modal-header">
                    <div class="title">
                        <p>Marcar presença</p>
                        <small>Marque uma ou mais presenças manualmente</small>
                    </div>
                    <div class="controls">
                        <button type="button" class="btn btn-light btn-close">Fechar</button>
                        <button type="button" class="btn btn-primary">Salvar</button>
                    </div>
                </div>`;

                var $headerHtml = $(headerHtml);

                $headerHtml.on("click", ".btn-close", function() {
                    bootbox.hideAll();
                });

                var box = bootbox.dialog({
                    title: $headerHtml,
                    message: $wrapperClone,
                    className: "marcar-presenca-modal",
                    size: "large"
                });
            };

            $btn.on("click", openMarcarPresencaModal);
            //openMarcarPresencaModal();

        });
 
    });

    $(".qrcode-image").each(function() {

        var $wrapper = $(this);
        
        var courseId = $(this).attr("data-course-id");
        var refreshTime = $(this).attr("data-refresh-time");
        
        var params = {};

        if ($wrapper.attr("data-size")) {
            params.size = $wrapper.attr("data-size");
        }

        if ($wrapper.attr("data-margin")) {
            params.margin = $wrapper.attr("data-margin");
        }

        if ($wrapper.attr("data-format")) {
            params.format = $wrapper.attr("data-format");
        }

        function refreshImage(forceRefresh) {

            var url = meta("url") + "/qrcode/" + courseId;

            params.force_refresh = forceRefresh ? 1 : 0;
            url = url + `?` + new URLSearchParams(params);

            fetch(url).then(function(response) {

                if (response.status == 200) {

                    response.text().then(function(source) {
                        $(".qrcode-hided-content").removeClass("d-none");
                        $wrapper.html(source);
                        setTimeout(refreshImage, refreshTime * 1000);
    
                    }).catch(function(err) {
                        console.error(err);
                        alert(`Não foi possível carregar o QR-Code, tentando novamente em ${refreshTime}seg`);
                        setTimeout(refreshImage, refreshTime * 1000);
                    });
                
                } else {

                    if (response.status == 400) {
                        $(".qrcode-hided-content").remove();

                    } else {
                        alert(`Não foi possível carregar o QR-Code, tentando novamente em ${refreshTime}seg`);
                        setTimeout(refreshImage, refreshTime * 1000);
                    }
                }
            });
        };

        refreshImage(true);

    });

    $(".page-courses.page-students-index").each(function() {

        var $page = $(this);
        var $coursesSummary = $page.find(".courses-summary");

        $page.find(".courses-table").each(function() {

            var $table = $(this);
            loadDatatable($table[0]);

            function openCourseDetails(course) {

                console.log(course);
                var rate = course.summary.attendances_rate;
                rate = rate.toLocaleString('pt-br');

                var absence = course.summary.total_attendances_until_now - course.summary.attendance;

                var html = `
                <div class="dates-data-wrapper">

                    <div class="cards courses-summary">
                        <div class="card card-body" data-key="current">
                            <h6>Presenças</h6>
                            <div class="text">
                                <h3>${ course.summary.attendance }</h3>
                                <small>${ course.summary.attendance }/${ course.summary.total_attendances_until_now }</small>
                            </div>
                        </div>
                        <div class="card card-body" data-key="next">
                            <h6>Faltas</h6>
                            <div class="text">
                                <h3>${ absence }</h3>
                                <small>${ absence }/${ course.summary.total_attendances_until_now }</small>
                            </div>
                        </div>
                        <div class="card card-body" data-key="last">
                            <h6>Percentual de presença</h6>
                            <div class="text">
                                <h3>${ rate }%</h3>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-actions attendances-table bigger-padding">
                            <thead>
                                <tr>
                                    <th>Aula</th>
                                    <th class="no-sort">Presença</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>`;

                var $html = $(html);
                var $table = $html.find("table");

                for (var date in course.datesData) {

                    let row = course.datesData[date];

                    if (row.future) {
                        continue;
                    }
    
                    var d1 = moment(date);
                    var d1str = d1.format("DD/MM/YY");
                    var d2 = row.date ? moment(row.date).format("HH:mm") : "-";

                    var dotClassName = 'dot-0';

                    if ((row.attendance / row.total) > 0.5) {
                        dotClassName = 'dot-2';
                    
                    } else if ((row.attendance / row.total) > 0) {
                        dotClassName = 'dot-1';
                    }
    
                    var tr = `
                    <tr>
                        <td data-sort="${ d1.format("YYYY-MM-DD")}">${ d1str }</td>
                        <td>${ d2 }</td>
                        <td data-sort="${ row.attendance }">
                            <div class="student-attendance-info">
                                <span>${ row.attendance }/${ row.total }</span>
                                <span class="dot ${ dotClassName }"></span>
                            </div>
                        </td>
                    </tr>`;
    
                    $table.find("tbody").prepend(tr);
                }

                // Carregar datatable
                loadDatatable($table[0]);

                var headerHtml = `
                <div class="custom-modal-header">
                    <div class="title">
                        <p>${ course.name } - ${ course.schedule_key }</p>
                        <small>${ course.user_name }</small>
                    </div>
                    <div class="controls">
                        <button type="button" class="btn btn-primary btn-refresh">Atualizar</button>
                        <button type="button" class="btn btn-light btn-close">Fechar</button>
                    </div>
                </div>`;

                var $headerHtml = $(headerHtml);

                $headerHtml.on("click", ".btn-close", function() {
                    bootbox.hideAll();
                });

                var box = bootbox.dialog({
                    title: $headerHtml,
                    message: $html,
                    className: "students-data-modal",
                    size: "large",
                    closeButton: false
                });

            };

            $table.on("click", "tbody tr", function() {
                openCourseDetails($(this).data("course")); return false;
            });

            $coursesSummary.on("click", ".card", function() {
                var name = $(this).attr("data-course");
                var $tr = $table.find(`tbody tr[data-name="${ name }"]`);
                $tr.trigger("click");
            });

        });

        var n = 0;

        function openQrcodeReader() {

            function qrcodeError(err) {

                bootbox.hideAll();

                setTimeout(function() {

                    var html = `
                    <div class="session-alert-content">
                        <div class="session-alert-head">
                            <div class="icon"><i class="fas fa-exclamation-circle"></i></div>
                            <h3 class="title">Atenção!</h3>
                        </div>
                        <div class="session-alert-text"></div>
                    </div>`;

                    var $html = $(html);
                    $html.find(".session-alert-text").html(`<p>Não foi possível obter acesso a sua câmera:</p><p><b>${ err }</b></p>`);

                    bootbox.dialog({
                        size: "small",
                        className: `session-alert-modal warning`,
                        message: $html,
                        closeButton: false,
                        buttons: { confirm: { label: "OK" } }
                    });

                });
            }

            if ('mediaDevices' in navigator && 'getUserMedia' in navigator.mediaDevices) {

                console.log("Let's get this party started")
                
                var headerHtml = `
                <div class="custom-modal-header">
                    <div class="title">
                        <p>Leitura do QR-code</p>
                        <small>Faça a leitura de um QR-code para marcar presença em uma aula</small>
                    </div>
                    <div class="controls">
                        <button type="button" class="btn btn-light btn-close">Fechar</button>
                    </div>
                </div>`;
    
                var $headerHtml = $(headerHtml);
    
                $headerHtml.on("click", ".btn-close", function() {
                    bootbox.hideAll();
                });
    
                var html = `
                <div class="qrcode-reader-wrapper">
                    <div class="canvas-wrapper not-loaded">
                        <div class="canvas-wrapper">
                            <canvas class="camera-output"></canvas>
                        </div>
                    </div>
                    <div class="d-none">
                        <div class="content"></div>
                        <button type="button" class="btn btn-primary btn-resume">Resume</button>
                        <button type="button" class="btn btn-primary btn-pause">Pause</button>
                    </div>
                </div>`;
    
                var $html = $(html);

                var $canvas = $html.find("canvas");
                $canvas.parent().wait();

                var $content = $html.find(".content");
                
                var canvasElement = $canvas[0];
                var canvas = canvasElement.getContext("2d");
    
                var box = bootbox.dialog({
                    title: $headerHtml,
                    message: $html,
                    className: "qrcode-reader-modal",
                    closeButton: false
                });
    
                var video = document.createElement("video");
                var streamInstance;

                function drawLine(begin, end, color) {
                    canvas.beginPath();
                    canvas.moveTo(begin.x, begin.y);
                    canvas.lineTo(end.x, end.y);
                    canvas.lineWidth = 4;
                    canvas.strokeStyle = color;
                    canvas.stroke();
                }

                function tick() {
                        
                    if (video.paused == false) {

                        if (video.readyState === video.HAVE_ENOUGH_DATA) {
    
                            /* loadingMessage.hidden = true;
                            canvasElement.hidden = false;
                            outputContainer.hidden = false; */
                    
                            canvasElement.height = video.videoHeight;
                            canvasElement.width = video.videoWidth;
                            canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);
    
                            var imageData = canvas.getImageData(0, 0, canvasElement.width, canvasElement.height);
    
                            var code = jsQR(imageData.data, imageData.width, imageData.height, {
                                inversionAttempts: "dontInvert",
                            });
    
                            if (code) {

                                pauseVideo();
                                console.log(code);
                                $content.html(code.data);

                                drawLine(code.location.topLeftCorner, code.location.topRightCorner, "#FF3B58");
                                drawLine(code.location.topRightCorner, code.location.bottomRightCorner, "#FF3B58");
                                drawLine(code.location.bottomRightCorner, code.location.bottomLeftCorner, "#FF3B58");
                                drawLine(code.location.bottomLeftCorner, code.location.topLeftCorner, "#FF3B58");

                                console.log(box);

                                $(box).find(".modal-content").wait();

                                setTimeout(function() {

                                    var nr = Math.floor((Math.random() * 100) + 1);
                                    console.log(nr);

                                    if (n == 0) {
                                        n++;
                                        requestCallback("success");

                                    } else {
                                        requestCallback("error", "Sua presença já foi registrada anteriormente!");
                                    }

                                }, 1 * 1000);
                                
                                return;
    
                            } else {
                                $content.html('Nenhum código encontrado');
                            }
                        }
    
                        requestAnimationFrame(tick);
                    }
                }

                function pauseVideo() {

                    video.pause();

                    if (streamInstance) {

                        streamInstance.getTracks().forEach(function(track) {
                            track.stop();
                        });
                    }
                }
    
                function resumeVideo() {

                    navigator.mediaDevices.getUserMedia({ video: { facingMode: "environment" } })
                    .then(function(stream) {

                        // Fechar wait
                        $canvas.parent().closeWait();

                        streamInstance = stream;
                        video.srcObject = stream;
                        video.setAttribute("playsinline", true); // required to tell iOS safari we don't want fullscreen
                        video.play();

                        // Requisitar frama
                        requestAnimationFrame(tick);

                    })
                    .catch(function(error) {
                        console.error(error);
                        qrcodeError('É necessário conceder permissão de acesso a sua câmera para continuar');
                    });
                }

                function requestCallback(type, message) {

                    bootbox.hideAll();

                    setTimeout(function() {

                        var sessionAlertIcons = {
                            success: '<i class="fas fa-check-circle"></i>',
                            error: '<i class="fas fa-times-circle"></i>',
                            warning: '<i class="fas fa-exclamation-circle"></i>'
                        };
                    
                        var html = `
                        <div class="session-alert-content">
                            <div class="session-alert-head">
                                <div class="icon">${ sessionAlertIcons[type] }</div>
                                <h3 class="title">${ type == 'success' ? 'Sucesso!' : 'Atenção!' }</h3>
                            </div>
                            <div class="session-alert-text"></div>
                        </div>`;
        
                        var $html = $(html);
    
                        if (type == "success") {
                            $html.find(".session-alert-text").html(`<p>Sua presença foi salva com sucesso!</p>`);
    
                        } else {
                            $html.find(".session-alert-text").html(`<p>Não foi possível salvar sua presença:</p><p><b>${ message }</b></p>`);
                        }
    
                        let buttons = {};
    
                        if (type == "error") {
    
                            buttons.tryAgain = {
                                label: "Tentar novamente",
                                className: "btn-outline-primary",
                                callback: function() {
                                    openQrcodeReader();
                                }
                            }
                        }
    
                        buttons.confirm = {
                            label: "OK",
                            className: "btn-primary"
                        }
        
                        bootbox.dialog({
                            size: "small",
                            className: `session-alert-modal ${ type }`,
                            message: $html,
                            closeButton: false,
                            buttons: buttons
                        });

                    });
                }

                $html.find(".btn-resume").on("click", function() {
                    resumeVideo();
                });

                $html.find(".btn-pause").on("click", function() {
                    pauseVideo();
                })
                
                box.on("shown.bs.modal", function() {
                    resumeVideo();
                });
    
                box.on("hide.bs.modal", function() {
                    pauseVideo();
                });

            } else {
                qrcodeError('Não foi possível identificar uma câmera no seu dispositivo');
            }
        }

        //openQrcodeReader();

        $page.find(".btn-read-qrcode").on("click", function() {
            openQrcodeReader();
        })

    });

});