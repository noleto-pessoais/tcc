<div class="session-alerts">

    @php
        $sessions = [ "success", "error", "warning" ];
    @endphp
    
    @foreach ($sessions as $s)
    
        @if (Session::has($s))
            <div class="alert-data" data-type="{{ $s }}">{!! Session::get($s) !!}</div>
        @endif
    
    @endforeach
    
    @if (count($errors) > 0)
        <div class="alert-data" data-type="error">{!! $errors->all()[0] !!}</div>
    @endif
    
</div>