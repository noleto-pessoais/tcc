@extends('layouts.app', [
    'hideHeader' => true
])

@section('content')
<div class="page page-courses page-qrcode" data-course="{{ $course->id }}">

    <div class="qrcode-wrapper">

        <div class="title">
            <h4>{{ $data->program_name }}</h4>
            <h3>{{ $data->name }} — {{ $data->schedule_key }}</h3>
            <p>{{ dateFormat($data->date, "d M") }}, {{ substr($data->start_at, 0, 5) }} às {{ substr($data->end_at, 0, 5) }}</p>
        </div>

        <div class="qrcode-image" data-course-id="{{ $course->id }}" data-refresh-time="{{ $qrCodeRefreshTime }}" data-size="450">
        </div>

    </div>

</div>
@endsection