@extends('layouts.app')

@section('content')
<div class="page page-courses page-details" data-course="{{ $course->id }}">

    @php
        $is = $user->is();
    @endphp

    <input type="hidden" data-json="userIs" value="{{ json_encode($is) }}" />

    <div class="page-title">

        <ul class="breadcrumb_">
            <li>Dashboard</li>
            <li>Minhas Turmas</li>
            <li>#{{ $course->id }}</li>
        </ul>

        <h3>{{ $course->name }}</h3>

    </div>

    @include("partials.alert")

    <div class="row">

        <div class="col-12 col-md-3 qrcode-column">

            <div class="qrcode-card card qrcode-hided-content d-none">

                <div class="card-header">
                    <h6 class="pt-1 pb-1">QR-Code</h6>
                    <!-- <button class="btn btn-icon" type="button"><i class="fas fa-cog"></i></button> -->
                </div>
                
                <div class="card-body">

                    <div class="qrcode-image" data-course-id="{{ $course->id }}" data-refresh-time="{{ $qrCodeRefreshTime }}">
                    </div>

                </div>

            </div>

            <div class="card-column-buttons mb-4 mb-md-0">
                <a target="_blank" href="{{ url('turma/'.$course->id.'/qrcode') }}" type="button" class="btn btn-primary qrcode-hided-content d-none">Abrir QR-Code</a>
                <button type="button" class="btn btn-outline-primary marcar-presenca-btn">Marcar presença</button>
            </div>

        </div>

        <div class="col-12 col-md-9">

            {{-- <div class="cards courses-summary">
        
                <div class="card card-body" data-key="current">
                    <h6>Aula atual</h6>
                    <div class="text"></div>
                </div>
        
                <div class="card card-body" data-key="next">
                    <h6>Próxima aula</h6>
                    <div class="text"></div>
                </div>
        
                <div class="card card-body" data-key="last">
                    <h6>Última aula</h6>
                    <div class="text"></div>
                </div>
                
            </div> --}}

            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">

                <li class="nav-item">
                    <a class="nav-link active" id="pills-aulas-tab" data-toggle="pill" href="#pills-aulas" role="tab" aria-controls="pills-aulas" aria-selected="true">Aulas ministradas e agendadas</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id="pills-students-tab" data-toggle="pill" href="#pills-students" role="tab" aria-controls="pills-students" aria-selected="true">Alunos matriculados</a>
                </li>
        
            </ul>
        
            <div class="tab-content" id="pills-tabContent">
        
                <div class="tab-pane fade" id="pills-students" role="tabpanel" aria-labelledby="pills-students-tab">

                    <div class="card">
        
                        <div class="card-header">
                            <div class="card-title">
                                <h4>Alunos matriculados</h4>
                            </div>
                        </div>
                
                        <div class="card-body">

                            <div class="table-responsive">

                                <table class="table datatable table-actions students-table">
    
                                    <thead>
                                        <tr>
                                            <th>Aluno</th>
                                            <th>Matrícula</th>
                                            <th>Presença</th>
                                            <th class="no-sort">Ações</th>
                                        </tr>
                                    </thead>
    
                                    <tbody>

                                        @foreach ($students as $s)
                                        <tr data-student="{{ json_encode($s) }}" class="{{ $s->summary['attendance_rate'] < 50 ? 'attendance-danger' : '' }}">
                                            <td>{{ $s->name }}</td>
                                            <td>{{ $s->matriculation }}</td>
                                            <td data-order="{{ $s->summary['attendance_rate'] }}">{{ $s->summary['attendance'].'/'.$s->summary['total'] }} — {{ $s->summary['attendance_rate'].'%' }}</td>
                                            <td>
                                                <div class="dropdown table-action-dropdown">
                                                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <div class="dropdown-icon"><i class="flaticon-more"></i></div>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <h6 class="dropdown-header">{{ $s->name }}</h6>
                                                        <a href="javascript:void(0)" class="dropdown-item open-student-data">Informações detalhadas</a>
                                                        <a href="javascript:void(0)" class="dropdown-item">Desmatricular</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
    
                                    </tbody>
    
                                </table>

                            </div>
                
                        </div>
                
                    </div>
        
                </div>

                <div class="tab-pane fade show active" id="pills-aulas" role="tabpanel" aria-labelledby="pills-aulas-tab">
        
                    <div class="card">
        
                        <div class="card-header">
                            <div class="card-title">
                                <h4>Aulas ministradas e agendadas</h4>
                            </div>
                        </div>
                
                        <div class="card-body">
                            
                        <div class="table-responsive">

                            <table class="table table-actions dates-table">

                                <thead>
                                    <tr>
                                        <th>Dia</th>
                                        <th>Presenças</th>
                                        <th class="no-sort">Ações</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    @php
                                        $todayDate = date('Y-m-d')
                                    @endphp

                                    @foreach ($dates as $d)

                                        @php
                                            $isToday = $d['day'] == $todayDate;
                                            $isFuture = $d['day'] > $todayDate;
                                            $dateStr = dateFormat($d['day'], 'd M y, D');
                                        @endphp

                                        <tr data-day="{{ json_encode($d) }}" class="{{ $isToday ? 'today' : '' }} {{ $isFuture ? 'future' : '' }}">
                                            <td class="date-column" data-sort="{{ $d['day'] }}">{{ $dateStr }}</td>
                                            <td>{{ $d['summary']['students_attendance'] }}/{{ $d['summary']['students_count'] }} — {{ $d['summary']['attendances_rate'].'%' }}</td>
                                            <td>
                                                <div class="dropdown table-action-dropdown">
                                                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <div class="dropdown-icon"><i class="flaticon-more"></i></div>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <h6 class="dropdown-header">{{ $dateStr }}</h6>
                                                        <a href="javascript:void(0)" class="dropdown-item open-date-data">Informações detalhadas</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                    @endforeach

                                </tbody>

                            </table>

                            <textarea class="d-none" name="dates-data">{{ json_encode($dates) }}</textarea>

                        </div>
                
                    </div>
        
                </div>
                
            </div>

        </div>

    </div>

    <div class="marcar-presenca-wrapper d-none">

        <form>

            <div class="table-responsive">
            
                <table class="table table-actions">
                
                    <thead>
                        <tr>
                            <!-- <th>Nome</th> -->
                            <th>Matricula</th>
                            <th>Data da presença</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
    
                    <tbody>
                        <tr>
                            <!-- <td>
                                <div class="form-group mb-0">
                                    <input type="text" class="form-control" name="name[]" placeholder="Nome do aluno" />
                                </div>
                            </td> -->
                            <td>
                                <div class="form-group mb-0">
                                    <input type="text" class="form-control" name="matriculation[]" placeholder="Matrícula aluno" />
                                </div>
                            </td>
                            <td>
                                <div class="form-group mb-0">
                                    <input type="datetime-local" class="form-control" name="date[]" />
                                </div>
                            </td>
                            <td>
                                <div class="table-buttons">
                                    <button type="button" title="Duplicar" class="btn btn-icon duplicate"><i class="far fa-clone"></i></button>
                                    <button type="button" title="Remover" class="btn btn-icon remove"><i class="fas fa-times"></i></button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
    
                </table>

            </div>

            <div class="controls d-flex">
                <button type="button" class="btn btn-primary add-nova-linha ml-auto">Adicionar nova linha</button>
            </div>

        </form>

    </div>

</div>
@endsection