@extends('layouts.app')

@section('content')
<div class="page page-courses page-index">

    @php
        $is = $user->is();
    @endphp

    <input type="hidden" data-json="programs" value="{{ json_encode($programs) }}" />

    <div class="page-title">

        <ul class="breadcrumb_">
            <li>Dashboard</li>
            <li>Minhas Turmas</li>
        </ul>

        <h3>Minhas Turmas</h3>

    </div>

    <div class="cards courses-summary">

        <div class="card card-body" data-key="current">
            <h6>Aula atual</h6>
            <div class="text"></div>
        </div>

        <div class="card card-body" data-key="next">
            <h6>Próxima aula</h6>
            <div class="text"></div>
        </div>

        <div class="card card-body" data-key="last">
            <h6>Última aula</h6>
            <div class="text"></div>
        </div>
        
    </div>

    @include("partials.alert")

    <div class="card">

        <div class="card-header">
            <div class="card-title">
                <h4>{{ $is["student"] ? 'Turmas matriculadas' : 'Turmas cadastradas' }}</h4>
            </div>
            <div class="card-buttons">
                @if ($is["student"])
                    <button type="button" class="btn btn-primary btn-register">Matricular em uma Turma</button>
                @else
                    <button type="button" class="btn btn-primary btn-add">Nova turma</button>
                @endif
            </div>
        </div>

        <div class="card-body">

            @if (count($courses) > 0)

                <div class="table-responsive">

                    <table class="table bigger-padding m-0">

                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nome</th>
                                <th>Curso</th>
                                <th>Horário</th>
                                <th>Qtde. de alunos</th>
                                <th>Ações</th>
                            </tr>
                        </thead>

                        <tbody>

                            @foreach ($courses as $c)
                            <tr data-course="{{ $c }}">
                                <td data-key="id">{{ $c->id }}</td>
                                <td>{{ $c->name }}</td>
                                <td>{{ $c->program_name }}</td>
                                <td>{{ $c->schedule_key }}</td>
                                <td>{{ random_int(15, 30) }}</td>
                                <td>
                                    <div class="table-buttons">
                                        <button type="button" class="btn btn-icon btn-update"><i class="fas fa-pen"></i></button>
                                        <button type="button" class="btn btn-icon btn-delete"><i class="fas fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>

                    </table>

                </div>

                {!! $courses->appends(Request::except('pages'))->links() !!}

            @else

                <div class="empty-courses">

                    <h3>Nenhuma turma encontrada</h3>

                    @if ($is["professor"])
                        <p>Cadastre uma nova turma e comece a gerenciar as presenças dos alunos pela plataforma</p>
                    
                    @elseif ($is["student"])
                        <p>Você pode se matricular em uma turma fazendo a leitura do qr-code no horário da aula</p>
                    @endif

                </div>

            @endif

        </div>

    </div>

</div>
@endsection