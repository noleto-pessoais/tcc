@extends('layouts.app')

@section('content')
<div class="page page-courses page-index page-students-index">

    <div class="page-title">

        <ul class="breadcrumb_">
            <li>Dashboard</li>
            <li>Minhas Turmas</li>
        </ul>

        <h3>Minhas Turmas</h3>

    </div>

    <div class="cards courses-summary">
        
        <div class="card card-body" data-key="current" data-course="Cálculo 3">
            <h6>Aula atual</h6>
            <div class="text">
                <h3>Cálculo 3</h3>
                <small>Marcelo Ferro</small>
                <small>24T12 - 28/04 14:00</small>
            </div>
        </div>
        
        <div class="card card-body" data-key="next" data-course="Compiladores 1">
            <h6>Próxima aula</h6>
            <div class="text">
                <h3>Compiladores 1</h3>
                <small>Deborah Silva</small>
                <small>35N23 - 29/04 18:50</small>
            </div>
        </div>
        
        <div class="card card-body" data-key="last" data-course="Pesquisa Operacional">
            <h6>Última aula</h6>
            <div class="text">
                <h3>Pesquisa Operacional</h3>
                <small>Cassio Dener</small>
                <small>7M3456 - 25/04 08:50</small>
            </div>
        </div>
        
    </div>

    @include("partials.alert")

    <div class="card">

        <div class="card-header">
            <div class="card-title">
                <h4>Turmas matriculadas</h4>
            </div>
            <div class="card-buttons">
                <button type="button" class="btn btn-primary btn-read-qrcode">Ler QR-Code</button>
            </div>
        </div>

        <div class="card-body">

            @if (count($courses) > 0)

                <div class="table-responsive">

                    <table class="table bigger-padding courses-table m-0">

                        <thead>
                            <tr>
                                <th>Turma</th>
                                <th>Professor(a)</th>
                                <th>Horário</th>
                                <th>Presenças</th>
                            </tr>
                        </thead>

                        <tbody>

                            @foreach ($courses as $c)
                            <tr data-name="{{ $c['name'] }}" data-course="{{ json_encode($c) }}">
                                <td>{{ $c["name"] }}</td>
                                <td>{{ $c["user_name"] }}</td>
                                <td>{{ $c["schedule_key"] }}</td>
                                <td data-order="{{ $c["summary"]['attendances_rate'] }}">{{ $c["summary"]['attendance'].'/'.$c["summary"]['total_attendances_until_now'] }} — {{ $c["summary"]['attendances_rate'].'%' }}</td>
                            </tr>
                            @endforeach

                        </tbody>

                    </table>

                </div>

            @else

                <div class="empty-courses">
                    <p><b>Nenhuma turma foi encontrada</b></p>
                    <p>Você pode se matricular em uma turma fazendo a leitura do qr-code no horário da aula</p>
                </div>

            @endif

        </div>

    </div>

</div>
@endsection