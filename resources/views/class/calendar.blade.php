@extends('layouts.app', [ 'bodyClass' => 'card-100' ])
@section('content')

<div class="page page-class page-calendar">
	
	@include('partials.alert')

	<div class="card">

		<input type="hidden" data-json="dates" value="{{ json_encode($dates) }}" />
		<input type="hidden" data-json="class" value="{{ json_encode($class) }}" />

		<div class="card-header">
			<div class="card-title">
				<span class="subtitle">Calendário</span>
				<span class="title">{{ $class->name }}</span>
			</div>
		</div>
		
		<div class="card-body">

			<div class="scroll-area">
				<div class="scroll-area-inner">
					<div id="calendar"></div>
				</div>
			</div>

			<div id='calendar'></div>

			<div class="page-controls">

				<div class="buttons">
					<a href="{{ url('turmas') }}" class="btn btn-link">Voltar</a>
				</div>
					
			</div>
		
		</div>
		
	</div>

</div>

@endsection
