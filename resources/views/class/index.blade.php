@extends('layouts.app')
@section('content')

<div class="page page-class page-index">

    @include('partials.alert')

    <div class="card">

        <div class="card-header">
            <div class="card-title">
                <span class="subtitle">Registros cadastrados</span>
                <span class="title">Turmas</span>
            </div>
        </div>

        <div class="card-body">

            <div class="table-area">

                @if (count($classes))

                    <div class="table-responsive">

                        <table class="table table-actions">

                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nome</th>
                                    <th>Período</th>
                                    <th>Horário</th>
                                    <th>Quant. alunos</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>

                            <tbody>

                                @foreach ($classes as $class)
                                <tr>
                                    <td>{{ $class->id }}</td>
                                    <td>{{ $class->name }}</td>
                                    <td>{{ $class->semester->key }}</td>
                                    <td>{{ $class->schedule_key }}</td>
                                    <td>{{ count($class->students) }}</td>
                                    <td>
                                        <div class="dropdown table-action-dropdown">
                                            <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <div class="dropdown-icon"><i class="flaticon-more"></i></div>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <h6 class="dropdown-header">{{ $class->name }}</h6>
                                                <a href="{{ url('turmas/'.$class->id.'/editar') }}" class="dropdown-item">Editar</a>
                                                <a href="{{ url('turmas/'.$class->id.'/calendario') }}" class="dropdown-item">Calendário</a>
                                                <a href="" class="dropdown-item btn-remove" data-id="{{ $class->id }}" data-url="/turmas">Remover</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>

                        </table>

                    </div>

                @else
                    <div class="obs">Nenhum registro foi encontrado.</div>
                @endif

                @php
                    $pagination = $classes->appends(Request::except('page'))->links();
                @endphp

            </div>

            <div class="page-controls">

                @if ($pagination)
                    <div class="pagination-wrapper">{!! $pagination !!}</div>
                @endif

                <div class="buttons">
                    <a href="{{ url('turmas/criar') }}" class="btn btn-primary">Nova turma</a>
                </div>
                    
            </div>

        </div>

    </div>

</div>

@endsection