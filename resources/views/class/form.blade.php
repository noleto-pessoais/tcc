@extends('layouts.app')
@section('content')

<div class="page page-class page-form">
	
	@include('partials.alert')

	@php
		$isCreate = empty($class->id);
		$title = $isCreate ? "Nova turma" : "Editar turma";
	@endphp

	<form method="POST" action="{{ url('turmas') }}" data-parsley-validate>

		{{ csrf_field() }}
		{{ method_field($isCreate ? 'POST' : 'PUT') }}

		<input type="hidden" name="id" value="{{ old('id', $class->id) }}" />
	
		<div class="card">
	
			<div class="card-header">
				<div class="card-title">
					<span class="subtitle">Formulário</span>
					<span class="title">{{ $title }}</span>
				</div>
			</div>
			
			<div class="card-body">

				<div class="row">

					<div class="form-group col-12 col-md-4">
						<label>Nome</label>
						<input type="text" class="form-control" name="name" value="{{ old('name', $class->name) }}" required />
					</div>

					<div class="form-group col-12 col-sm-6 col-md-4">
						<label>Período</label>
						<select name="semester_id" class="form-control" required>
							
							@foreach ($semesters as $semester)
								<option value="{{ $semester->id }}" {{ $semester->id == old('semester_id', $class->semester_id) ? 'selected' : '' }}>{{ $semester->key }}</option>
							@endforeach

						</select>
					</div>

					<div class="form-group col-12 col-sm-6 col-md-4">
						<label>Horário</label>
						<input type="text" class="form-control" name="schedule_key" value="{{ old('schedule_key', $class->schedule_key) }}" required />
					</div>

				</div>

				@php

					$students = [];
					
					if (old('student_code')) {
						
						foreach (old('student_code') as $k => $v) {

							array_push($students, (object) [
								'student_code' => $v,
								'name' => old('student_name')[$k]
							]);
						}
						
					} else {
						$students = $class->students;
					}

				@endphp

				<input type="hidden" name="has_students" value="{{ count($students) > 0 ? 1 : '' }}" required data-parsley-errors-container="#students-error" data-parsley-required-message="É obrigatório cadastrar ao menos um aluno!	" />

				<div class="table-responsive">
					
					<table class="table table-fixed students-table">

						<thead>
							<tr>
								<th>#</th>
								<th>Matrícula</th>
								<th>Nome</th>
								<th>Ações</th>
							</tr>
						</thead>

						<tbody>

							@foreach ($students as $index => $student)

								@php
									$j = $index + 1;
								@endphp

								<tr>
									<td>{{ $j }}</td>
									<td>
										<div class="form-group">
											<input type="tel" class="form-control" name="student_code[]" value="{{ $student->student_code }}" placeholder="{{ 'Matrícula aluno '.$j }}" />
										</div>
									</td>
									<td>
										<div class="form-group">
											<input type="text" class="form-control" name="student_name[]" value="{{ $student->name }}" placeholder="{{ 'Nome aluno '.$j }}" />
										</div>
									</td>
									<td>
										<button title="Remover aluno" class="btn btn-iconn text-danger btn-remove-student" type="button"><i class="flaticon-close"></i></div></button>
									</td>
								</tr>

							@endforeach

							<tr class="new-student-tr">
								<td colspan="4">
									<div class="new-student-wrapper">
										<button type="button" class="btn btn-sm btn-link new-student-btn">Adicionar aluno</button>
										<div class="form-group" id="students-error"></div>
									</div>
								</td>
							</tr>
							
						</tbody>

					</table>

				</div>


				<div class="page-controls">
	
					<div class="buttons">
						<a href="{{ url('turmas') }}" class="btn btn-link">Voltar</a>
						<button type="submit" class="btn btn-primary">Salvar</a>
					</div>
						
				</div>
			
			</div>
			
		</div>

	</form>
	
</div>

@endsection
