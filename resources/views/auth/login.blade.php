@extends('layouts.blank', ['bodyClass' => 'vertical-align'])

@section('content')
<div class="page login">

    <div class="page-image">
        <img class="group-image" data-id="3" src="{{ asset('assets/img/studying.svg') }}" />
    </div>

    <div class="form-content">

        <div class="page-title">
            <h4>Entrar</h4>
            {{-- <p>Gerenciamento de presenças</p> --}}
        </div>

        <form method="post" action="{{ url('login') }}" data-parsley-validate>

            @csrf

            <div class="form-groups">

                <div class="form-group">
                    <label>Login <small>Faça o login com seu e-mail, matrícula ou login</small></label>
                    <input type="text" class="form-control" name="login" value="{{ old('login') }}" required autofocus />
                </div>

                <div class="form-group">
                    <label>Senha</label>
                    <input type="password" class="form-control" name="password" required />
                </div>

            </div>

            <div class="form-actions">
                <button type="submit" class="btn btn-primary">Entrar</button>
                <p>Não possui uma conta? <a href="{{ url('register') }}">Registre-se</a></p>
            </div>

        </form>

    </div>

</div>
@endsection