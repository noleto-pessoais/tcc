@extends('layouts.blank', ['bodyClass' => 'vertical-align'])

@section('content')
<div class="page register">

    <div class="page-image">
        <img class="group-image d-none" data-id="2" src="{{ asset('assets/img/teaching.svg') }}" />
        <img class="group-image" data-id="3" src="{{ asset('assets/img/exams.svg') }}" />
    </div>

    <div class="form-content">

        <div class="page-title">
            <h4>Crie uma nova conta</h4>
            <p>Mauris finibus at dolor quis cursus</p>
        </div>
    
        <form method="post" action="{{ url('register') }}" data-parsley-validate>
    
            @csrf
    
            @php
                $groupId = old('group_id') ?? 3;
            @endphp
    
            <div class="form-groups">
    
                <div class="form-group">
                    <label>Nome</label>
                    <input type="text" class="form-control" name="name" maxlength="255" value="{{ old('name') }}" required autofocus />
                </div>
    
                <div class="form-group">
    
                    <label class="mb-2">Eu sou um...</label>
    
                    <div class="sys-checkbox-list horizontal">
                        
                        <div class="sys-checkbox mb-1">
                            <input id="groupId3" type="radio" name="group_id" value="3" {{ $groupId == 3 ? 'checked' : '' }} />
                            <span></span><label for="groupId3">Aluno</label>
                        </div>
    
                        <div class="sys-checkbox mb-1">
                            <input id="groupId2" type="radio" name="group_id" value="2" {{ $groupId == 2 ? 'checked' : '' }} />
                            <span></span><label for="groupId2">Professor</label>
                        </div>
    
                    </div>
    
                </div>
    
                <div class="form-group">
                    <label>Curso</label>
                    <select name="program_id" class="form-control" required>
                        <option value="">Selecione</option>
                        @foreach ($programs as $p)
                            <option value="{{ $p->id }}" {{ old('program_id') == $p->id ? 'selected' : '' }}>{{ $p->name }}</option>
                        @endforeach
                    </select>
                </div>
    
                <div class="form-group">
                    <label>Matrícula</label>
                    <input type="text" class="form-control" name="matriculation" value="{{ old('matriculation') }}" maxlength="20" required />
                </div>
    
                <div class="form-group">
                    <label>E-mail</label>
                    <input type="email" class="form-control" name="email" maxlength="255" value="{{ old('email') }}" required />
                </div>
    
                <div class="form-group">
                    <label>Login<small>É com esse nome que você fará login na plataforma</small></label>
                    <input type="text" class="form-control" name="login" maxlength="20" value="{{ old('login') }}" required />
                </div>
    
                <div class="form-group">
                    <label>Senha</label>
                    <input type="password" class="form-control" name="password" data-parsley-minlength="6" maxlength="12" required />
                </div>
    
            </div>
    
            <div class="form-actions">
                <button type="submit" class="btn btn-primary">Criar nova conta</button>
                <p>Já tenho uma conta? <a href="{{ url('login') }}">Faça o login</a></p>
            </div>
    
        </form>

    </div>

</div>
@endsection
