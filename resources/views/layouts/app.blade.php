<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('partials.head')

@php

	$classes = [];

	if (isset($bodyClass)) {
		array_push($classes, $bodyClass);
	}

@endphp

<body class="{{ implode(' ', $classes) }}">

	<div id="app">

		@php
			$user = Auth::user();
		@endphp

		@if (!isset($hideHeader))

			<header>

				<div class="header-center">

					<div class="pages">
						<div class="pg-item {{ Route::current()->uri == 'turmas' ? 'active' : '' }}"><a class="pg-link" href="{{ url('turmas') }}">Minhas turmas</a></div>
						<div class="pg-item"><a class="pg-link btn-logout" href="">Sair</a></div>
					</div>

				</div>

				<div class="header-right">

				</div>

			</header>

		@endif

		<main>
			<div class="container">@yield('content')</div>
		</main>

	</div>

	<script src="{{ asset('assets/js/vendor.min.js') }}"></script>
	<script src="{{ asset('assets/js/scripts.min.js') }}"></script>

</body>

</html>