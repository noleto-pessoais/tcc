<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('partials.head')

<body class="blank {{ $bodyClass ?? '' }}">

    <div id="app">

		@include("partials.alert")

        <main>
			<div class="container">@yield('content')</div>
		</main>

	</div>

	<script src="{{ asset('assets/js/vendor.min.js') }}"></script>
	<script src="{{ asset('assets/js/scripts.min.js') }}"></script>
	
</body>
</html>
