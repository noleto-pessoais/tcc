<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("group_id");
            $table->string('name', 255);
            $table->string('email')->unique();
            $table->string("login")->unique();
            $table->string('password');
            $table->unsignedBigInteger("program_id")->nullable();
            $table->string("matriculation")->nullable()->unique();
            $table->boolean("is_active")->default(true);
            $table->rememberToken();
            $table->timestamps();
            $table->foreign("program_id")->references("id")->on("programs")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }
}
