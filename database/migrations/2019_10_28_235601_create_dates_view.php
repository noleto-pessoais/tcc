<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatesView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        $sql = "
        CREATE VIEW dates AS (
            SELECT i::date as date, extract(dow from i::date) as weekday
            FROM generate_series('2019-01-01', '2021-12-31', '1 day'::interval) i
        )";

        DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::statement('DROP VIEW IF EXISTS dates');
    }
}
