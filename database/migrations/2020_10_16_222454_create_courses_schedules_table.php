<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses_schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("course_id");
            $table->unsignedBigInteger('schedule_id');
            $table->integer("weekday");
            $table->foreign("course_id")->references("id")->on("courses")->onDelete("cascade");
            $table->foreign("schedule_id")->references("id")->on("schedules")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses_schedules');
    }
}
