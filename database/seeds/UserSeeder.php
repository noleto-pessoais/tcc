<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder {
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        
        $users = [
            [
                "group_id" => 1,
                "name" => "Victor",
                "email" => "victor@mail.com",
                "login" => "victor",
                "password" => bcrypt("123123")
            ],
            [
                "group_id" => 1,
                "name" => "João",
                "email" => "joao@mail.com",
                "login" => "joao",
                "password" => bcrypt("123123")
            ],
            [
                "group_id" => 2,
                "name" => "Marcelo",
                "email" => "marcelo@mail.com",
                "login" => "marcelo",
                "password" => bcrypt("123123")
            ],
        ];

        User::insert($users);
    }
    
}
