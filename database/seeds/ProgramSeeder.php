<?php

use App\Models\Program;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProgramSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        DB::statement("TRUNCATE programs CASCADE");

        $programs = [
            ["id" => 1, "name" => "Engenharia de Computação"],
            ["id" => 2, "name" => "Engenharia Elétrica"],
            ["id" => 3, "name" => "Engenharia Civil"],
            ["id" => 4, "name" => "Engenharia Mecânica"],
            ["id" => 5, "name" => "Engenharia Ambiental e Sanitária"]
        ];

        Program::insert($programs);
    }
}
