<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {

        // Bases estáticas
        $this->call(ScheduleSeeder::class);
        $this->call(ProgramSeeder::class);
        
        // Bases dinâmicas
        $this->call(UserSeeder::class);
        $this->call(CourseSeeder::class);
    }
    
}
