<?php

use App\Models\Schedule;
use Illuminate\Database\Seeder;

class ScheduleSeeder extends Seeder {
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $schedules = [

            // Horários da manhã
            [ "key" => "M1", "start_at" => "07:10", "end_at" => "08:00" ],  // 1
            [ "key" => "M2", "start_at" => "08:00", "end_at" => "08:50" ],  // 2
            [ "key" => "M3", "start_at" => "08:50", "end_at" => "09:40" ],  // 3
            [ "key" => "M4", "start_at" => "10:00", "end_at" => "10:50" ],  // 4
            [ "key" => "M5", "start_at" => "10:50", "end_at" => "11:40" ],  // 5
            [ "key" => "M6", "start_at" => "11:40", "end_at" => "12:30" ],  // 6

            // Horáriops da tarde
            [ "key" => "T1", "start_at" => "13:10", "end_at" => "14:00" ],  // 7
            [ "key" => "T2", "start_at" => "14:00", "end_at" => "14:50" ],  // 8
            [ "key" => "T3", "start_at" => "14:50", "end_at" => "16:00" ],  // 9
            [ "key" => "T4", "start_at" => "16:00", "end_at" => "16:50" ],  // 10
            [ "key" => "T5", "start_at" => "16:50", "end_at" => "17:40" ],  // 11
            [ "key" => "T6", "start_at" => "17:40", "end_at" => "18:30" ],  // 12

            // Horários da noite
            [ "key" => "N1", "start_at" => "18:05", "end_at" => "18:50" ],  // 13
            [ "key" => "N2", "start_at" => "18:50", "end_at" => "19:35" ],  // 14
            [ "key" => "N3", "start_at" => "19:35", "end_at" => "20:30" ],  // 15
            [ "key" => "N4", "start_at" => "20:30", "end_at" => "21:15" ],  // 16
            [ "key" => "N5", "start_at" => "21:15", "end_at" => "22:00" ]   // 17

        ];

        Schedule::insert($schedules);
    }

}
