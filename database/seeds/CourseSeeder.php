<?php

use App\Models\Course;
use App\Traits\CourseTrait;
use Illuminate\Database\Seeder;

class CourseSeeder extends Seeder {

    use CourseTrait;

    /**
     * Run the database seeds.
     * @return void
     */
    public function run() {

        $courses = [
            [
                "name" => "Conversão Eletromecânica de Energia 1",
                "program_id" => 2,
                "schedule_key" => "24T56"
            ],
            [
                "name" => "Direito para Engenharia",
                "program_id" => 1,
                "schedule_key" => "2N45"
            ],
            [
                "name" => "Linguagens Formais",
                "program_id" => 1,
                "schedule_key" => "2M12"
            ],
            [
                "name" => "Resistência dos Materiais I",
                "program_id" => 3,
                "schedule_key" => "5M3456"
            ],
            /* [
                "name" => "Sistemas de Comunicações",
                "program_id" => 1,
                "schedule_key" => "35N45"
            ], */
        ];

        Course::truncate();

        foreach ($courses as $c) {

            $course = Course::make($c);
            $course->user_id = 2;
            $course->save();

            $schedules = $this->getSchedulesFromKey($course->schedule_key);

            if ($schedules) {

                foreach ($schedules as $s) {
                    $course->schedules()->attach($s["schedule_id"], [ "weekday" => $s["weekday"] ]);
                }

            } else {
                dd($course);
            }
        }
    }
}
