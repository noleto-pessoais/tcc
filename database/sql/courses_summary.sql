select
	c.id, c.name, c.schedule_key,
	cs.weekday,
	d.date,
	s.start_at, s.end_at
from courses as c
inner join courses_schedules as cs on cs.course_id = c.id
inner join schedules as s on s.id = cs.schedule_id
inner join dates as d on d.weekday = cs.weekday and (date > (CURRENT_DATE - interval '1 week') and date <= (CURRENT_DATE + interval '1 week'))