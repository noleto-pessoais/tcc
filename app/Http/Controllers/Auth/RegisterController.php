<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use App\Models\Program;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $programs = Program::all();
        return view('auth.register', ["programs" => $programs]);
    }

    /**
     * Handle a registration request for the application.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request) {

        $validator = $this->validator($request);

        if ($validator->fails()) {
            return back()->withInput()->withErrors('<p>Não foi possível concluir o seu registro: <span class="font-400">'.$validator->errors()->first().'</span></p>');
        
        } else {

            try {
                
                $user = $this->create($request);
                Auth::login($user);
                
                Session::flash("success", "Usuário criado com sucesso!");
                return redirect($this->redirectPath());

            } catch (\Exception $e) {
                return back()->withInput()->withErrors('Não foi possível concluir o seu registro: '.$e->getMessage());
            }
        }
    }

    /**
     * Retornar validação
     * @param Request $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'group_id' => ['required', 'numeric'],
            'program_id' => ['nullable', 'required_if:group_id,3', 'exists:programs,id'],
            'matriculation' => ['nullable', 'required_if:group_id,3', 'string', 'max:20', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'login' => ['required', 'alpha_num', 'max:20', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'max:12'],
        ]);

        return $validator;
    }

    /**
     * Criar usuário
     * @param Request $request
     * @return \App\Models\User
     */
    protected function create(Request $request) {
        $user = new User();
        $user->name = $request->name;
        $user->group_id = $request->group_id;
        $user->matriculation = $request->matriculation ?? null;
        $user->email = $request->email;
        $user->login = $request->login;
        $user->password = Hash::make($request->password);
        $user->save();
        return $user;
    }
}
