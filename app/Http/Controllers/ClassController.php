<?php

namespace App\Http\Controllers;

use App\Models\ClassStudent;
use App\Models\Grade;
use App\Models\Meeting;
use App\Models\Semester;
use App\Traits\ClassTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ClassController extends Controller {

    use ClassTrait;

    public function test(Request $request) {

        $key = $request->key ?? "35M56";

        $schedules = $this->getSchedulesFromKey($key);

        dd($schedules);


    }

    public function index(Request $request) {

        $user = $request->user();
        $classes = Grade::getFromUser($user)->limit(10)->paginate(10);

        return view("class.index", [
            "user" => $user,
            "classes" => $classes
        ]);
    }

    public function create() {
        return $this->form(new Grade());
    }

    public function edit(Request $request, $id) {

        $class = Grade::getFromUser($request->user(), $id);

        if ($class) {
            return $this->form($class);

        } else {
            return back()->withErrors("Turam não encontrada.");
        }
    }

    private function form(Grade $class) {

        return view("class.form", [
            "semesters" => Semester::get(),
            "class" => $class
        ]);
    }

    public function save(Request $request) {

        $validator = Validator::make($request->all(), [
            "name" => "required|string",
            "semester_id" => "required|numeric|exists:ufg_academic_semesters,id",
            "schedule_key" => "required|string"
        ]);

        $validator->sometimes("id", "required|numeric|exists:classes", function($request) {
            return $request->_method == "PUT";
        });

        $validator->after(function($validator) use ($request) {
            
            if ($request->id) {

                $grade = Grade::getFromUser($request->user(), $request->id);

                if (!$grade) {
                    $validator->errors()->add("id", "Turma não encontrada / sem permissão de acesso.");
                }
            }

            $studentsCodes = $request->student_code;
            $studentsNames = $request->student_name;

            if ($studentsCodes && $studentsNames) {

                if (count($studentsCodes) == count($studentsNames)) {

                    for ($i = 0; $i < count($studentsCodes); $i++) {

                        $code = $studentsCodes[$i];
                        $name = $studentsNames[$i];

                        if (!$code || !$name) {
                            $validator->errors()->add("student_code", "É obrigatório especificar a matrícula e nome de todos os alunos desejados.");
                        }
                    }

                } else {
                    $validator->errors()->add("student_code", "Especifique a mesma quantidade de matrículas e nomes para os alunos.");    
                }

            } else {
                $validator->errors()->add("student_code", "É obrigatório cadastrar ao menos um aluno.");
            }

            $schedules = $this->getSchedulesFromKey($request->schedule_key);

            if ($schedules == null) {
                $validator->errors()->add("schedule_key", "A chave de horário não é válida. Verifique o valor informado.");
            }

        });

        if ($validator->fails() == false) {

            DB::beginTransaction();

            try {

                if ($request->id) {
                    $grade = Grade::getFromUser($request->user(), $request->id);

                } else {
                    $grade = new Grade();
                }

                $grade->user_id = $request->user()->id;
                $grade->name = $request->name;
                $grade->schedule_key = $request->schedule_key;
                $grade->semester_id = $request->semester_id;
                $grade->save();

                $students = [];
                for ($i = 0; $i < count($request->student_code); $i++) {

                    array_push($students, [
                        "class_id" => $grade->id,
                        "student_code" => $request->student_code[$i],
                        "name" => $request->student_name[$i],
                    ]);
                }

                ClassStudent::where("class_id", $grade->id)->delete();
                ClassStudent::insert($students);

                $schedules = $this->getSchedulesFromKey($request->schedule_key);
                $grade->schedules()->detach();

                foreach ($schedules as $scheduleItem) {
                    $grade->schedules()->attach($scheduleItem["schedule_id"], [ "weekday" => $scheduleItem["weekday"] ]);
                }

                Session::flash("success", "Turma salva com sucesso!");

                DB::commit();
                return redirect("turmas");

            } catch (\Exception $e) {

                DB::rollBack();
                return back()->withError('<small>Erro interno</small>'.$e->getMessage());
                
            }

        } else {
            return back()->withError($validator->errors()->first());
        }
    }

    public function delete(Request $request) {

        $validator = Validator::make($request->all(), [
            "id" => "required|numeric|exists:classes",
        ]);

        $validator->after(function($validator) use ($request) {

            $grade = Grade::getFromUser($request->user(), $request->id);

            if (!$grade) {
                $validator->errors()->add("id", "Turma não encontrada ou sem permissão de acesso.");
            }

        });

        if ($validator->fails() == false) {

            try {

                $grade = Grade::find($request->id);
                $grade->delete();

                Session::flash("success", "Turma removida com sucesso!");
                return redirect("turmas");

            } catch (\Exception $e) {
                return back()->withError($e->getMessage());
            }

        } else {
            return back()->withError($validator->errors()->first());
        }
    }

    // --------------------------------------------------------------------------------------
    // Acima estão os métodos relacionados ao CRUD
    // Abaixo estão os métodos relacionadas a subpáginas de Turmas

    public function calendar(Request $request, $id) {

        $class = Grade::getFromUser($request->user(), $id);

        if ($class) {
            return view("class.calendar", [ "class" => $class, "dates" => $class->calendar(0) ]);

        } else {
            return back()->withErrors("Turam não encontrada.");
        }
    }

}
