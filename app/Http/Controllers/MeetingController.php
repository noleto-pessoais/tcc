<?php

namespace App\Http\Controllers;

use App\Models\Meeting;
use Illuminate\Http\Request;

class MeetingController extends Controller {

    public function index(Request $request) {
        return view("meeting.index", [ "meetings" => Meeting::limit(10)->paginate() ]);
    }

    public function create() {
        return $this->form(new Meeting());
    }

    public function edit($id) {

        $meeting = Meeting::find($id);

        if ($meeting) {
            return $this->form($meeting);

        } else {
            return back()->withErrors("Evento não encontrado.");
        }
    }

    private function form(Meeting $meeting) {
        return view("meeting.form", ["meeting" => $meeting]);
    }

    public function save(Request $request) {

        if ($request->id) {
            $meeting = Meeting::find($request->id);

        } else {
            $meeting = new Meeting();
        }

        $meeting->name = $request->name;
        $meeting->started_at = $request->started_at.":00";
        $meeting->ended_at = $request->ended_at.":59";
        $meeting->save();

        return redirect("/eventos");

    }

    public function qrcode($id) {

        $meeting = Meeting::find($id);

        if ($meeting) {
            return view("meeting.qrcode", ["meeting" => $meeting]);

        } else {
            return back()->withErrors("Evento não encontrado.");
        }
    }

}
