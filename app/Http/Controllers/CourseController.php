<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Program;
use App\Traits\CourseTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Carbon\Carbon;
use Faker;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;

class CourseController extends Controller {

    use CourseTrait;

    public $qrCodeRefreshTime = 60;

    public function index(Request $request) {

        $user = Auth::user();

        if ($user->group_id == 2) {

            $orderedColumns = ['id', 'name', 'program_name', 'schedule_key'];
            $column = checkOrderBy($orderedColumns, $request->column, "id");
    
            $limit = $request->display_qty ?? 10;
            $sort = $request->sort ?? "desc";
    
            $programs = Program::all();
            $courses = Course::getByUser($user, $request)->orderBy($column, $sort)->paginate($limit);
    
            return view("courses.index", [
                "user" => $user,
                "courses" => $courses,
                "programs" => $programs
            ]);

        } else {

            $courses = [
                [
                    "name" => "Compiladores 1",
                    "user_name" => "Deborah Silva",
                    "schedule_key" => "35N23"
                ],
                [
                    "name" => "Pesquisa Operacional",
                    "user_name" => "Cassio Dener",
                    "schedule_key" => "7M3456"
                ],
                [
                    "name" => "Cálculo 3",
                    "user_name" => "Marcelo Ferro",
                    "schedule_key" => "24T12"
                ],
                [
                    "name" => "Laboratório de Física 4",
                    "user_name" => "Jose Ricardo",
                    "schedule_key" => "6N23"
                ],
            ];

            $startDate = new Carbon ("2021-02-22");
            $endDate = new Carbon("2021-06-12");

            $now = now()->format("Y-m-d");

            $schedulesTimes = [
                "M1" => [ "start_at" => "07:10", "end_at" => "08:00" ],  // 1
                "M2" => [ "start_at" => "08:00", "end_at" => "08:50" ],  // 2
                "M3" => [ "start_at" => "08:50", "end_at" => "09:40" ],  // 3
                "M4" => [ "start_at" => "10:00", "end_at" => "10:50" ],  // 4
                "M5" => [ "start_at" => "10:50", "end_at" => "11:40" ],  // 5
                "M6" => [ "start_at" => "11:40", "end_at" => "12:30" ],  // 6
                "T1" => [ "start_at" => "13:10", "end_at" => "14:00" ],  // 7
                "T2" => [ "start_at" => "14:00", "end_at" => "14:50" ],  // 8
                "T3" => [ "start_at" => "14:50", "end_at" => "16:00" ],  // 9
                "T4" => [ "start_at" => "16:00", "end_at" => "16:50" ],  // 10
                "T5" => [ "start_at" => "16:50", "end_at" => "17:40" ],  // 11
                "T6" => [ "start_at" => "17:40", "end_at" => "18:30" ],  // 12
                "N1" => [ "start_at" => "18:05", "end_at" => "18:50" ],  // 13
                "N2" => [ "start_at" => "18:50", "end_at" => "19:35" ],  // 14
                "N3" => [ "start_at" => "19:35", "end_at" => "20:30" ],  // 15
                "N4" => [ "start_at" => "20:30", "end_at" => "21:15" ],  // 16
                "N5" => [ "start_at" => "21:15", "end_at" => "22:00" ]   // 17
            ];
            
            foreach ($courses as &$c) {

                $copyDate = $startDate->copy();
                $dates = [];
                $totalDatesUntilNow = 0;
                $totalAttendancesUntilNow = 0;

                $schedules = $this->getSchedulesFromKey($c["schedule_key"]);
                $weekdays = array_unique(array_column($schedules, "weekday"));
                $weekdayMultiplier = [];

                foreach ($schedules as $s) {

                    $w = $s["weekday"];

                    if (isset($weekdayMultiplier[$w])) {
                        $weekdayMultiplier[$w]++;

                    } else {
                        $weekdayMultiplier[$w] = 1;
                    }
                }

                while ($copyDate <= $endDate) {

                    $w = $copyDate->dayOfWeek;

                    if (in_array($w, $weekdays)) {

                        if ($copyDate <= $now) {
                            $totalDatesUntilNow++;
                            $totalAttendancesUntilNow += $weekdayMultiplier[$w];
                        }

                        array_push($dates, $copyDate->copy());
                    }

                    $copyDate->addDay();
                }

                $c["summary"] = [
                    "attendance" => 0,
                    "attendances_rate" => 0,
                    "total_attendances_until_now" => $totalAttendancesUntilNow
                ];
                
                $datesData = [];

                foreach ($dates as $dateObject) {

                    $d = $dateObject->format("Y-m-d");
                    $w = $dateObject->dayOfWeek;

                    if ($d < $now) {

                        $datesData[$d] = [
                            "day" => $d,
                            "future" => false,
                            "attendance" => 0,
                        ];
            
                        $studentDate = null;
                        $time = 0;
                        $attendance = 0;
                        
                        $dSchedules = array_filter($schedules, function($item) use ($w) {
                            return $item["weekday"] == $w;
                        });

                        $dTimes = array_values(array_map(function($item) {
                            return substr($item["key"], 1, 2);
                        }, $dSchedules));

                        $randomTimeIndex = 0;

                        while (true) {

                            if ($randomTimeIndex >= count($dTimes)) {
                                $randomTimeIndex = -1; break;
                            }

                            if (d100(80)) {
                                break;
                                
                            } else {
                                $randomTimeIndex++;
                            }
                        }

                        if ($randomTimeIndex >= 0) {

                            $randomTime = $dTimes[$randomTimeIndex];
                            $scheduleTimeInfo = $schedulesTimes[$randomTime];

                            $attendance = count($dTimes) - $randomTimeIndex;

                            $scheduleTimeStartDate = new Carbon($d." ".$scheduleTimeInfo["start_at"]);
                            $time = random_int(0, 49);
                            $studentDate = $scheduleTimeStartDate->addMinutes($time)->format("Y-m-d H:i:s");
                        }
    
                        $datesData[$d] = [
                            "date" => $studentDate,
                            "attendance" => $attendance,
                            "total" => count($dTimes)
                        ];
    
                        if ($attendance > 0) {
                            $c["summary"]["attendance"] += $attendance;
                        }
                        
                        $dateAttendanceRate = round(( $c["summary"]["attendance"] / $totalAttendancesUntilNow) * 100, 2);
                        $c["summary"]["attendances_rate"] = $dateAttendanceRate;
        
                    } else {
                        $datesData[$d]["future"] = true;
                    }
                }

                $c["datesData"] = $datesData;
            }

            return view("courses.students-index", [
                "user" => $user,
                "courses" => $courses,
            ]);
        }
    }

    /**
     * Course save
     * @param Request $request
     * @return void
     */
    public function save(Request $request) {

        DB::beginTransaction();

        try {

            $validator = Validator::make($request->all(), [
                "name" => "required|string",
                "program_id" => "required|numeric|exists:programs,id",
                "schedule_key" => "required|string"
            ]);
    
            $validator->sometimes("id", "required|numeric|exists:courses", function($request) {
                return $request->_method == "PUT";
            });
    
            $user = $request->user();
            $schedules = $this->getSchedulesFromKey($request->schedule_key);

            if ($validator->fails()) {
                return response($validator->errors()->first(), Response::HTTP_BAD_REQUEST);
    
            } else {

                if ($request->id) {

                    $course = Course::findByUser($user, $request->id);
    
                    if ($course == null) {
                        return response("Sem permissão de acesso para essa turma", Response::HTTP_BAD_REQUEST);                }

                } else {
                    $course = new Course();
                }
    
                if ($schedules == null) {
                    return response("A chave de horário não é válida. Verifique o valor informado.", Response::HTTP_BAD_REQUEST);
                    
                } else {
        
                    $course->user_id = $request->user()->id;
                    $course->name = $request->name;
                    $course->program_id = $request->program_id;
                    $course->schedule_key = $request->schedule_key;
                    $course->save();
        
                    $course->schedules()->detach();
        
                    foreach ($schedules as $s) {
                        $course->schedules()->attach($s["schedule_id"], [ "weekday" => $s["weekday"] ]);
                    }
        
                    DB::commit();
                    Session::flash("success", "Turma salva com sucesso!");
                    return response($course->id);
                }
            }

        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Course delete
     * @param Request $request
     * @return void
     */
    public function delete(Request $request) {

        $error = "Não foi possível remover o usuário:<br/><b>{message}</b>";

        try {

            if ($request->id) {
    
                $user = Auth::user();
                $course = Course::findByUser($user, $request->id);
    
                if ($course) {

                    $course->delete();

                    Session::flash("success", "Turma removida com sucesso!");

                    return redirect("turmas");
    
                } else {
                    $message = "Curso não encontrado ou sem permissão de acesso";
                }
    
            } else {
                $message = "Requisição inválida";
            }

        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        $error = str_replace("{message}", $message, $error);
        return back()->withError($error);
    }

    /**
     * Retornar quais são as aulas atual, próxima e última
     * @param Request $request
     * @return []
     */
    public function reminder(Request $request) {
        return Course::reminder($request);
    }

    public function details(Request $request, $id) {

        $startDate = new Carbon ("2021-02-22");
        $endDate = new Carbon("2021-06-12");
        $copyDate = $startDate->copy();
        $dates = [];

        while ($copyDate <= $endDate) {

            if ($copyDate->dayOfWeek == 2 || $copyDate->dayOfWeek == 4) {
                array_push($dates, $copyDate->format("Y-m-d"));
            }

            $copyDate->addDay();
        }

        $faker = Faker\Factory::create();
        //$faker->addProvider(new Faker\Provider\pt_BR\Person($faker));

        $students = [];

        for ($i = 0; $i < 30; $i++) {

            $matriculation = "20".random_int(13, 20).random_int(1, 9).random_int(10, 99);

            $student = (object) [
                "name" => $faker->name,
                "matriculation" => $matriculation,
                "attendances" => [],
                "summary" => [
                    "attendance" => 0,
                    "absence" => 0
                ]
            ];

            $students[$matriculation] = $student;
        }

        $datesData = [];
        $now = now()->format("Y-m-d");

        // Horário usado de base: 35N23 (terça e quinta, 18h50 às 20h20)
        foreach ($dates as $d) {

            $datesData[$d] = [
                "day" => $d,
                "future" => false,
                "students" => [],
                "summary" => [
                    "students_attendance" => 0,
                    "students_count" => count($students),
                    "attendances_rate" => 0
                ]
            ];

            if ($d < $now) {

                foreach ($students as $s) {
    
                    $studentDate = null;
                    $time = 0;
                    $attendance = 0;
    
                    // 90% de chance do aluno ter as duas presenças
                    if (d100(70)) {
                        $time = random_int(0, 20);
                        $attendance = 2;
    
                    // 70% de chance do aluno ter as duas faltas
                    } else if (d100(50)) {
                        $time = random_int(50, 65);
                        $attendance = 1;
                    }
    
                    if ($time > 0) {
                        $studentDate = (new Carbon($d." 18:50:00"))->addMinutes($time)->format("Y-m-d H:i:s");
                    }

                    // Cada aula equivale a duas presenças
                    $absence = 2 - $attendance;
    
                    $datesData[$d]["students"][$s->matriculation] = [
                        "date" => $studentDate,
                        "attendance" => $attendance,
                        "matriculation" => $s->matriculation,
                        "name" => $s->name
                    ];

                    if ($attendance > 0) {
                        $datesData[$d]["summary"]["students_attendance"]++;
                    }

                    $dateAttendanceRate = round(( $datesData[$d]["summary"]["students_attendance"] /  $datesData[$d]["summary"]["students_count"]) * 100, 2);
                    $datesData[$d]["summary"]["attendances_rate"] = $dateAttendanceRate;

                    $students[$s->matriculation]->attendances[$d] = [
                        "date" => $studentDate,
                        "attendance" => $attendance
                    ];
                    
                    $students[$s->matriculation]->summary["attendance"] += $attendance;
                    $students[$s->matriculation]->summary["absence"] += $absence;

                    $studentTotal = $students[$s->matriculation]->summary["attendance"] + $students[$s->matriculation]->summary["absence"];
                    $studentAttendanceRate = round(($students[$s->matriculation]->summary["attendance"] / $studentTotal) * 100, 2);

                    $students[$s->matriculation]->summary["total"] = $studentTotal;
                    $students[$s->matriculation]->summary["attendance_rate"] = $studentAttendanceRate;
                }

            } else {
                $datesData[$d]["future"] = true;
            }
        }

        $user = Auth::user();
        $course = Course::find($id);

        return view("courses.details", [
            "user" => $user,
            "course" => $course,
            "students" => $students,
            "dates" => $datesData,
            "qrCodeRefreshTime" => $this->qrCodeRefreshTime
        ]);
    }

    public function qrcode(Request $request, $id) {

        $user = Auth::user();
        $course = Course::find($id);

        if ($course && $course->id && $course->user_id == $user->id) {

            $reminder = Course::reminder($request);
            $currentOrNext = $reminder ? ($reminder["current"] ?? $reminder["next"]) : null;

            if ($currentOrNext) {

                return view("courses.qrcode", [
                    "user" => $user,
                    "course" => $course,
                    "data" => $currentOrNext,
                    "qrCodeRefreshTime" => $this->qrCodeRefreshTime
                ]);

            } else {
                return redirect("turmas")->withErrors("Nenhuma aula ativa ou futura encontrada para a turma");
            }

        } else {
            return redirect("turmas")->withErrors("Sem permissão de acesso para a turma");
        }
    }

    public function getQrcodeImage(Request $request, $id) {
        
        $user = Auth::user();
        $course = Course::find($id);
        $user = Auth::user();

        if ($course && $course->id && $course->user_id == $user->id) {

            $cacheKey = 'course-qrcode-'.$course->id;

            if (Cache::has($cacheKey) == false || $request->force_refresh) {

                $request->merge([
                    "course_id" => $course->id
                ]);
    
                $reminder = Course::reminder($request);
                $currentOrNext = $reminder ? ($reminder["current"] ?? $reminder["next"]) : null;

                if ($currentOrNext) {

                    $data = [
                        "id" => $id,
                        //"url" => url("turma/${id}/presenca"),
                        "expire_at" => now()->addSeconds(60)->format("Y-m-d H:i:s"),
                        //"start_at" => $currentOrNext->date." ".$currentOrNext->start_at,
                        //"end_at" => $currentOrNext->date." ".$currentOrNext->end_at,
                    ];

                    $json = json_encode($data);
                    $encryptedJson = Crypt::encryptString($json);
    
                    // Criar cache do conteúdo QR-code por 60seg
                    Cache::put($cacheKey, $encryptedJson, $this->qrCodeRefreshTime);
                
                } else {
                    return response("Nenhuma aula ativa ou futura encontrada para a turma", 400);
                }
            }

            $qrCodeContent = Cache::get($cacheKey);

            $size = $request->size ?? 250;
            $margin = $request->margin ?? 0;
            $format = $request->format ?? "svg";
            
            $qrcode = QrCode::format($format)->size($size)->margin($margin)->generate($qrCodeContent);
            return $qrcode;

        } else {
            return response("Sem permissão de acesso para essa turma", 405);
        }
    }
}
