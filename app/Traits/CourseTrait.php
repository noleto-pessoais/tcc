<?php

namespace App\Traits;

use App\Models\Schedule;

trait CourseTrait {

    /**
     * Retornar a referência dos horários de determinada chave
     * @param string $key
     * @return array
     */
    function getSchedulesFromKey($key) {

        $weekdays = [
            1 => 2, 2 => 3, 3 => 4, 4 => 5, 5 => 6, 6 => 7
        ];

        $references = Schedule::get()->keyBy("key");
        $referencesKeys = array_keys($references->toArray());

        $key = mb_strtoupper($key);
        $parts = explode(" ", $key);

        $schedules = [];

        foreach ($parts as $part) {

            $m = strpos($part, "M") ? 1 : 0;
            $v = strpos($part, "T") ? 1 : 0;
            $n = strpos($part, "N") ? 1 : 0;

            $check = $m + $v + $n;

            if ($check == 1) { 
                
                $split = $m ? 'M' : ($v ? 'T' : 'N');
                $temp = explode($split, $part);

                // Checar primeira parte: dias da semana
                $days = str_split($temp[0]);
                $daysIntersect = array_intersect($weekdays, $days);

                if (count($daysIntersect) == count($days)) {

                    // Checar segunda parte: horários do dia
                    $times = str_split($temp[1]);
    
                    $arr2 = array_map(function($x) use ($split) {
                        return $split.$x;
                    }, $times);
    
                    $intersect = array_intersect($referencesKeys, $arr2);

                    if (count($intersect) == count($arr2)) {

                        $merge = [];

                        foreach (array_keys($daysIntersect) as $weekdayIndex) {
                            foreach (array_keys($intersect) as $scheduleIndex) {

                                $scheduleArrKey = $referencesKeys[$scheduleIndex];
                                $ref = $references[$scheduleArrKey];

                                array_push($merge, [
                                    "schedule_id" => $ref->id,
                                    "weekday" => $weekdayIndex,
                                    "key" => $weekdays[$weekdayIndex].$ref->key
                                ]);
                            }
                        }

                        // Somar arrays
                        $schedules = array_merge($schedules, $merge);

                    // Algum horário inválido
                    } else {
                        return null;
                    }

                // Algum dia inválido
                } else {
                    return null;
                }

            } else {
                return null;                
            }
        }

        return $schedules;
    }

}