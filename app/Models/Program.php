<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Program model
 * @author Victor Noleto <victornoleto@sysout.com.br>
 * @since 08/11/2020 
 * @version 1.0.0
 */
class Program extends Model {

    protected $table = "programs";
    public $timestamps = false;
    
}
