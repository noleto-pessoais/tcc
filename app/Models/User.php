<?php

namespace App\Models;

use App\Enums\UserGroupEnum;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin() {
        return $this->group_id == UserGroupEnum::Admin;
    }

    public function firstname() {
        return explode(" ", $this->name)[0];
    }

    /**
     * Verificar se um usuário pertence a algum grupo
     * @param [type] $keys
     * @return boolean
     */
    public function is($check = null) {

        $userGroupId = $this->group_id;
        $returnBoolean = $check != null;

        if ($check == null) {
            $check = UserGroupEnum::getKeys();

        } else {

            if (!is_array($check)) {
                $check = explode("|", $check);
            }
        }

        $is = [];

        foreach ($check as $k) {

            $k = ucfirst($k);
            $groupId = UserGroupEnum::getValue($k);

            $x = mb_strtolower($k);
            $is[$x] = $groupId == $userGroupId;

            if ($is[$x] && $returnBoolean) {
                return true;
            }
        }

        if ($returnBoolean) {
            return false;

        } else {
            return $is;
        }
    }
}
