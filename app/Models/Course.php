<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Course extends Model {

    protected $table = "courses";

    public function schedules() {
        return $this->belongsToMany("App\Models\Schedule", "courses_schedules", "course_id")->withPivot("weekday");
    }

    public function students() {
        return $this->hasMany("App\Models\CourseStudent", "course_id");
    }

    /**
     * Get by user
     * @param [type] $query
     * @param User $user
     * @param Request $request
     * @return void
     */
    public function scopeGetByUser($query, User $user, Request $request) {
        return $this->courseQuery($query, $user, $request);
    }

    /**
     * Find by user
     * @param [type] $query
     * @param User $user
     * @param [type] $courseId
     * @return void
     */
    public function scopeFindByUser($query, User $user, $courseId) {

        $request = new Request(["id" => $courseId]);

        $query = $this->courseQuery($query, $user, $request);
        
        $user = $query->first();

        if ($user && $user->id) {
            return $user;
            
        } else {
            return null;
        }
    }

    /**
     * Course query
     * @param [type] $query
     * @param User $user
     * @param Request $request
     * @return void
     */
    private function courseQuery($query, User $user, Request $request) {

        $query->select("courses.*", "p.name as program_name");

        $query->join("programs as p", "p.id", "courses.program_id");

        if (!$user->isAdmin()) {
            $query->where("user_id", $user->id);
        }

        if ($request) {

            if ($request->id) {
                $query->where("courses.id", $request->id)->first();
            }
        }

        return $query;
    }

    public static function reminder(Request $request) {

        $user = $request->user();
        $courseId = $request->course_id ?? null;

        $sql = 
        "select
            c.id, c.name, c.schedule_key, p.name as program_name,
            cs.weekday,
            d.date,
            min(s.start_at) as start_at, max(s.end_at) as end_at,
            (d.date - CURRENT_DATE) as date_diff
        from courses as c
        inner join programs as p on p.id = c.program_id
        inner join courses_schedules as cs on cs.course_id = c.id
        inner join schedules as s on s.id = cs.schedule_id
        inner join dates as d on d.weekday = cs.weekday and (date > (CURRENT_DATE - interval '1 week') and date <= (CURRENT_DATE + interval '1 week'))
        where c.user_id = ".$user->id.
        ($courseId ? " and c.id = ${courseId}" : '').
        "group by c.id, p.id, cs.weekday, d.date
        order by d.date, start_at";

        $result = DB::select($sql);
        
        // Retorno: aula atual, próxima e última
        $data = [
            "current" => null,
            "next" => null,
            "last" => null
        ];

        if (count($result) > 0) {

            foreach ($result as $row) {

                // Aula anterior
                if ($row->date_diff < 0) {

                    if ($data["last"]) {

                        $diff = $row->date_diff - $data["last"]->date_diff;

                        // Se for maior (-4 > -5), substituir imediatamente
                        if ($diff > 0) {
                            $data["last"] = $row;

                        // Se for igual, comparar data de término
                        } else if ($diff == 0 && $row->end_at > $data["last"]->end_at) {
                            $data["last"] = $row;
                        }

                    } else {
                        $data["last"] = $row;
                    }

                // Próxima aula
                } else if ($row->date_diff > 0) {

                    if ($data["next"]) {
                        
                        $diff = $row->date_diff - $data["next"]->date_diff;

                        // Se for menor (1 < 2), substituir imediatamente
                        if ($diff < 0) {
                            $data["next"] = $row;

                        // Se for igual, comparar data de início
                        } else if ($diff == 0 && $row->start_at < $data["next"]->start_at) {
                            $data["next"] = $row;    
                        }

                    } else {
                        $data["next"] = $row;
                    }

                // Aula atual
                } else if ($row->date_diff == 0) {

                    $now = date("Y-m-d H:i:s");

                    if ($row->start_at <= $now && $row->end_at > $now) {

                        if (!$data["current"] || $row->start_at < $data["current"]->start_at) {
                            $data["current"] = $row;
                        }
                    }
                }
            }
        }

        return $data;
    }


}
