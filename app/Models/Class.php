<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Grade extends Model {

    protected $table = "classes";

    public function semester() {
        return $this->belongsTo("App\Models\Semester");
    }

    public function schedules() {
        return $this->belongsToMany("App\Models\Schedule", "classes_schedules", "class_id")->withPivot("weekday");
    }

    public function students() {
        return $this->hasMany("App\Models\ClassStudent", "class_id");
    }

    /**
     * Retornar turmas de determinado usuário (professor)
     * @param $query
     * @param User $user
     * @param integer $classId
     * $return $query
     */
    public function scopeGetFromUser($query, User $user, $classId = null) {

        $query->join("ufg_academic_semesters as s", "s.id", "classes.semester_id");

        $query->select(
            "classes.*",
            "s.started_at as semester_started_at", "s.ended_at as semester_ended_at"
        );

        if (!$user->isAdmin()) {
            $query->where("user_id", $user->id);
        }

        if ($classId) {
            return $query->where("classes.id", $classId)->first();
        
        } else {
            return $query;
        }
    }

    /**
     * Retornar todos os horários possíveis de determinada turma
     * @param boolean $group
     * @return array
     */
    public function calendar($group = true) {

        $sql = "
        SELECT
            d.date, d.weekday, min(us.started_at), max(us.ended_at)
        FROM classes AS c
        INNER JOIN ufg_academic_semesters AS uas on uas.id = c.semester_id
        INNER JOIN dates AS d ON d.date between uas.started_at AND uas.ended_at
        INNER JOIN classes_schedules AS cs ON cs.class_id = c.id AND cs.weekday = d.weekday
        INNER JOIN ufg_schedules AS us ON us.id = cs.schedule_id
        WHERE c.id = ".$this->id."
        GROUP BY d.date, d.weekday
        ORDER BY d.date, d.weekday";

        /*$sql = "
        SELECT
            d.*, us.started_at, us.ended_at
        FROM classes AS c
        INNER JOIN ufg_academic_semesters AS uas on uas.id = c.semester_id
        INNER JOIN dates AS d ON d.date between uas.started_at AND uas.ended_at
        INNER JOIN classes_schedules AS cs ON cs.class_id = c.id AND cs.weekday = d.weekday
        INNER JOIN ufg_schedules AS us ON us.id = cs.schedule_id
        ORDER BY d.date, us.started_at
        WHERE c.id = ".$this->id;*/

        return DB::select($sql);

    }

}
