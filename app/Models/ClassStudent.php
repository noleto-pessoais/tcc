<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassStudent extends Model {

    protected $table = "classes_students";
    public $timestamps = false;
    
}
