<?php

namespace App\Enums;
use BenSampo\Enum\Enum;

final class UserGroupEnum extends Enum {
    const Admin = 1;
    const Professor = 2;
    const Student = 3;
}
