<?php

use Carbon\Carbon;

if (!function_exists('checkOrderBy')) {

    /**
     * Verifica se a coluna de ordenação existe no array de colunas da tabela
     * @param array $arr
     * @param string $column
     * @param string $default
     * @return $value
     */
    function checkOrderBy($arr, $column, $default) {

        if (!empty($arr) && !empty($column) && in_array($column, $arr)) {
            return $column;

        } else {
            return $default;
        }
    }
}

function dateFormat($dateStr, $format = "d/m/y") {
    return (new Carbon($dateStr))->format($format);
}

function d100($check) {
    return random_int(1, 100) <= $check;
}
